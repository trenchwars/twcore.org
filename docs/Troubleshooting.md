<!-- Name: Troubleshooting -->
<!-- Version: 6 -->
<!-- Last-Modified: 2007/11/16 16:31:52 -->
<!-- Author: dugwyler -->

# Troubleshooting / problems with TWCore
You can't get it to work? You've tried everything you can think of but you keep getting the error? You are at the point of ripping your hair out and smack the computer?
Follow the following steps:

 1. Calm down
 1. Calm down some more
 1. Check the list below for common sources of trouble, and ensure that you have done everything that is asked.
 1. If after this you still have problems, visit the [/discussion Forum] and create a new topic with details on your problem/error, what you were trying to achieve and what you did trying to fixing it. The TWCore developers (and other forum members) will try to help you getting TWCore up and running.

If you want you can try to approach one of the developers in-game. However, expect that you will usually be redirected to the public forum if the problem/question is too extensive.


## Common sources of trouble
Before flipping out and killing people, ensure that:  

 1. You have the most recent copy of TWCore available.
 1. Your classpath is set up to a JDK properly.
 1. All .cfg files that you intend to use have the "example" part removed.
 1. TWCore's /bin/setup.cfg [DirectoriesCore Location] is set to the directory that setup.cfg resides in.
 1. You use Ant Build on build.xml to correctly build a jar file containing all core files, and compile all bots to on-the-fly loadable .class files.  You can do this by running ant build using the "all" option.  To use ant inside Eclipse, right click on build.xml, select Run As, and then Ant Build ... .  If this won't work, try using the bld or bld.bat script instead.  Either way should create a working twcore.jar file that can be run in the next step.
 1. (To reiterate the directions of the last step) you do NOT use your IDE to compile TWCore the way you would a different project!
 1. You start TWCore with runbots or runbots.bat to ensure that the proper includes are loaded (SQL support, the Google API that is no longer used, and the AIM API for interacting with bots over IM).
 1. You have RetryCount and ConnectionCount set to 0 in bin/corecfg/sql.cfg if you don't intend to use SQL support yet; or, if you do, that you have the credentials set up properly in sql.cfg, and ConnectionCount matches the number of connections you intend to run.  
If, after following all of these steps, you still have problems loading TWCore, please consult the forums or one of the developers in-game for further advice.


## Bugs
If you are certain you have found a bug in TWCore and you're not able (or willing) to fix it yourself, please create a [/newticket new ticket] specifying exactly the bug and what you did leading to this bug. Make sure you include which bot this concerns, which commands you did and any errors you got.  
If you have registered or specified your correct e-mail address in the ticket you will receive e-mail notifications as soon as someone replies to the ticket.
