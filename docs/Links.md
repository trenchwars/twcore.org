<!-- Name: Links -->
<!-- Version: 6 -->
<!-- Last-Modified: 2007/08/11 23:19:10 -->
<!-- Author: Maverick -->

# Links
This page contains use useful links related to TWCore and Java.


## TWCore-related links
 * [Trench Wars](http://www.trenchwars.org)   
 The website of SSCU Trench Wars, the zone where TWCore comes from.

 * [D1st0rtion](http://d1st0rt.sscentral.com)   
 The website of D1st0rt, one of the programmers of TWCore. His website contains various TWCore and other bot-related resources that's worth visiting.

## Continuum / Subspace related links
 * SubSpace 1.34 Protocol List [[RTF Document](http://kirk.sscentral.com/docs/PacketList.zip)] [[HTML](http://d1st0rt.sscentral.com/packets.html)]  
  _"This is a comprehensive list of all the packets used in the SubSpace 1.34 protocol. Since not all of these are implemented in the 1.3a release of the BotCore, you may need to add them yourself if you wish to use them. (Note: This list is not yet complete)."_ -Admiral Kirk


## Java-related links
 * [Java.sun.com](http://java.sun.com)   
   The home of Java. It contains anything you need related to Java - [Downloads](http://developers.sun.com/downloads/), [Tutorials](http://java.sun.com/docs/books/tutorial/), [Forums](http://forum.java.sun.com) etc.


  
Do you have links that should be here? You can add them yourself after you [/login login].