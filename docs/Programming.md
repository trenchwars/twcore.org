---
disable_toc: true
---
<!-- Name: Programming -->
<!-- Version: 35 -->
<!-- Last-Modified: 2014/05/21 11:36:16 -->
<!-- Author: Maverick -->


# Programming on/with TWCore
Do you want to make bots with TWCore? Do you want to make changes to the core of TWCore? Here is the guide to get you started.

To setup your IDE with TWCore the following steps are described:

 1. [Download Java](#download-java)
 1. [Install Java](#install-java)
 1. [Download Eclipse](#download-eclipse)
 1. [Install Eclipse](#install-eclipse)
 1. [Install Subclipse](#install-subclipse)
 1. [Checkout TWCore](#checkout-twcore)
 1. [Resolve compilation problems](#resolve-compilation-problems)
 1. [Run TWCore](#run-twcore)
 1. [Make 4 spaces on a tab](#make-4-spaces-on-a-tab)
 1. [Committing your changes](#committing-your-changes)

Note: This guide describes how to get you started using Eclipse and the Subclipse plugin. These are the recommended programs to use. It's possible that you prefer a different IDE and tools however this guide doesn't describe setting up with these other tools. You're welcome to add another guide though.

Do not be frightened by the length of this guide, most things will go rather quickly.

  
## Download Java
You need Java Software Development Kit (JDK) for developing TWCore. We currently use Java 7 to run TWCore in Trench Wars. It's recommended that you also use Java 7 for developing so we won't get any problems with java-version-specific features/changes.

Download JDK 7 here: [http://www.oracle.com/technetwork/java/javase/downloads/index.html](http://www.oracle.com/technetwork/java/javase/downloads/index.html).   
Don't choose the java version with netbeans IDE or Java EE, choose the "*Java Platform (JDK) XuXX*" download.
  
## Install Java
Installing Java is pretty straightforward; execute the .exe file you downloaded and follow the installation instructions. While Java is being installed, you might want to consider starting the next step, to prevent you from wasting your time.
  
## Download Eclipse
To download Eclipse go to [http://www.eclipse.org/downloads/](http://www.eclipse.org/downloads/) and choose the "*Eclipse IDE for Java Developers*" package. 

  
## Install Eclipse
Once downloaded, extract the 'eclipse' directory from the zip file into `C:\Program Files`. (It's recommended to use [7-zip](http://www.7-zip.org) if you need a program to extract zip files.)  
Create a shortcut at your desktop or in your start menu to `C:\Program Files\eclipse\eclipse.exe`. Please note that it is not mandatory to put Eclipse in your Program Files folder.

When you start Eclipse it will ask you the location of your workspace. You can choose to use the suggested default location but it's recommended to use `C:\Workspaces` or equivalent. This workspace is where Eclipse will store all your projects.  
*NOTE:* Due to a bug in Eclipse, it is advised to *not* name your workspace TWCore. This can lead to problems when you try to run it.


![image](img/Eclipse_welcome_screen.gif#left)

The first time you start eclipse it will display a welcome screen. You can close this screen by closing the Welcome tab or clicking on the silver/gold arrow on the right side of the window.

  
  
## Install Subclipse plugin
Subversion is the source code versioning system we use for the source files of TWCore. The subversion repository for the source code files is located at this website, under the directory [/svn](http://www.twcore.org/svn). To prevent you from having to manually download each file one at a time, we will be installing a plugin in Eclipse.

Subclipse is the plugin for Eclipse to work with Subversion repositories. It takes care of checking out, committing, updating, reverting code and much more. It works quite well when you know how to work with Subclipse (and Eclipse). Working with Subclipse is explained in [Contributing to TWCore](Contribute). (You don't need to worry about this yet though.)

There are two main methods as to how you can install a plugin, mainly dependent on the version of Eclipse that you are using.

![image](img/marketplace.png#right)

*Method 1*

 1. On the menu, click on Help / Eclipse Marketplace...;
 2. In the new window, enter in the search box 'subclipse';
 3. Press the install button for the Subclipse 1.10.4 as displayed in the image to the right;
 4. Ensure every box is checked and press the button 'Confirm >'.  
    If the same screen pops up again, simply press 'Confirm >' again;
 5. Select the radio button 'I accept the terms of the license agreements' and press the button 'Finish';
 6. Eclipse will now install the Subclipse plugin. This may take a few minutes.  
    If any warning boxes pop up, press 'Ok' to continue the installation;
 7. When finished, a prompt window will be shown asking you to restart Eclipse.  
    Click the 'Yes' button to restart Eclipse.


*Method 2*

 1. On the menu, click on Help / Software Updates...
 1. In the new window, click on the tab 'Available Software'
 1. On the right side of the window, click on the button 'Add Site...'
 1. Enter the URL `http://subclipse.tigris.org/update_1.8.x` and click on OK
 1. The URL you just entered has been added to the list of websites and automatically expanded.
 1. Under the URL are 3 components listed; Subclipse, Subclipse integrations and Subclipse SVNKit Option.  
    Select all checkboxes under the Subclipse component or select the entire Subclipse component.
 1. On the right side of the window, click the button 'Install...'
 1. A new window entitled 'Install' will be showed.
    1. Click 'Next' button
    1. Select the radiobutton before 'I accept the terms of the license agreements'
    1. Click 'Finish' button
 1. Eclipse will now install the Subclipse plugin.
 1. When finished, a prompt window will be showed asking you to restart Eclipse.  
    Click the 'Yes' button to restart Eclipse.



  
## Checkout TWCore
 1. Go to the "Subclipse Repository perspective": On the menu, click on Window / Open Perspective / Other... / "SVN Repository Exploring".
    
    Usually you may want to switch perspective using the perspective buttons at the top right of the window:   
    ![image](img/SVNPerspectiveButton.gif)


Note: You will be asked for your username/password when accessing the source repository. The username/password are the same as the credentials of your TWCore.org account. To prevent further prompts for your username and password, make sure to select the checkbox "Save Password".  

Follow the following steps to checkout TWCore:

 1. In the SVN Repository Exploring perspective, right-click on the left area ("SVN Repositories" tab), click New / Repository location...
 1. In the new window type the following URL: `http://www.twcore.org/svn` and press enter or click on "Finish".
 1. Once the repository has been added, click it open in the left panel by clicking on the [[+]] sign on the left of the URL
 1. Click on the [[+]] sign on the front of the trunk folder.
 1. Right click the twcore folder under the trunk folder and click on "Checkout..." in the menu.
 1. You should see the following window:  
 ![image](img/CheckoutFromSVN.gif)
 1. Click on "Finish".
 1. Eclipse will now checkout TWCore from the Subversion repository.
 1. Once done, switch back to the Java perspective.

  
## Resolve compilation problems
After checking out TWCore, there will most likely be a few kinds of compilation problems. This chapter describes solving the most common problems (and usually all of the problems).

 * If you have the following problems then you haven't set your Java JRE. Please note that the numbers between the square brackets might be different:  
 ![image](img/CompilationProblems1.gif)
  1. From the Java perspective, right-click the TWCore project and click on "Properties" (last option of the menu)
  1. In the new window click on "Java Build Path" in the left tree
  1. Click on the "Libraries" tab
  1. Select "JRE System Library jdk1.7.0_01" and click on the "Edit..." button on the right
  1. In the new window click the "Alternate JRE" radio button and click the "Installed JREs..." button on the right
  1. In the new window you'll see there is no jre1.7.0 in the list, you need to add it.
  1. Click the "Add..." button
  1. In the new window click on the "Browse..." button and navigate to the directory where you installed the JDK 7 earlier on (not the JRE directory) and click on OK.
  1. You'll notice that Eclipse has filled in most of the window. Click on OK again.
  1. In the "Installed JREs" window you'll see that jdk1.7.0_01 has been added to the list. Click on OK again.
  1. In the "JRE System Library" window select the jdk1.7.0_01 option from the list of Alternate JREs. Click on the Finish button.
  1. Click on OK.
  1. You should now see only warnings in the problems tab:  
  ![image](img/CompilationProblemsSolved.gif)
  The actual amount of warnings you will see depend on your access level. The public repository currently contains 0 warnings.
  
 * If you have the following problems then you only need to refresh and clean your project:  
 ![image](img/CompilationProblems2.gif)
  1. From the Java perspective, right-click the TWCore project and click on "Refresh".
  1. In the top menu, click on Project / Clean... 
  1. In the new window, select "Clean all projects" and click on OK.
  1. The problems should be gone and your problems tab should display something similar to:  
  ![image](img/CompilationProblemsSolved.gif)

  
## Run TWCore
Now that you have TWCore compiling successfully from within Eclipse, it's not that hard to get TWCore running from within Eclipse aswell.

Note: you need (access to) a subgame/biller server that run without problems for TWCore to connect to. Bots should be able to enter the server (server.ini setting) or you should have a few bot names that are on vip.txt and sysop.txt . The bots of TWCore need to be sysop since they don't return a security checksum so they can't stay connected without being logged into sysop.

Although optional, it is highly recommended that you also have (access to) a MySQL server for TWCore to connect to. Developers of the TW Bot Dev team can get access to the TWCore test database running on twcore.org.

 1. From within the Java perspective, lookup setup.example.cfg which is located in the `src` folder
 1. Select setup.example.cfg and press Ctrl+C and Ctrl+V to copy the file into the same directory.
 1. In the popup window enter the name "setup.cfg" and press OK.
 1. Double-click the new setup.cfg file and configure it. Some parameters are described in the [Running On Windows](RunningOnWindows#Configuration) guide but it should be self-explanatory.  
 Notice that you have to set the "Core Location" to the bin directory within your workspace. (For example: `C:\Workspace\TWCore\bin` .)

 1. Navigate to the corecfg package (under the src folder)
 1. Select all the files and press Ctrl+C and Ctrl+V
 1. Rename each file so that it doesn't have a ".example" part. (Mind that Eclipse puts "Copy of" in front of the filename.)
 1. Put your own nickname on `owners.cfg` and configure `sql.cfg`.
    a. For sql.cfg, if you have no access to a MySQL server, you should set the 'ConnectionCount' to 0. All other settings can stay unchanged.
    b. If you do have a MySQL server set up, then you will need to set up the proper user account(s) and password(s) in here.  
       Do note that the setting 'ConnectionCount' overrules the amount you've actually specified, and having this count set to a higher number than actual valid connections will lead to the situation where TWCore keeps attempting to connect to a non-existing database.

Now we have to set a single parameter before starting TWCore so that TWCore starts from the `\bin` directory.

 1. Navigate to the twcore.core package
 1. Right-click the Start.java file
 1. Click on Run As / Open Run Dialog...
 1. In the Run window double click "Java Application" from the tree left.
 1. If you see the following error then fix this error first by following the steps below:  
 ![image](img/RunError.gif)
 1. Click on the Arguments tab
 1. Under "Working directory:" select the "Other" radio button
 1. Type in: `${workspace_loc:TWCore\bin}`
 1. Click the Run button

A log tab will now show where you would see that TWCore is starting up; initializing SQL connections and connecting to the subgame server. If something goes wrong it will be displayed in the log window which is usually caused by bad configuration or a problem with the SQL/subgame server.
  
*Project compatibility error*

If you have the following error in the Run dialog then follow these steps to solve this:  
![image](img/RunError.gif)
![image](img/JavaCompilerCompliance.gif#right)

 1. If you are still in the Run Dialog, click the Close button to close the window
 1. From the Java perspective, right-click the TWCore project
 1. Click on "Properties" (bottom of the menu)
 1. In the Properties window, click on "Java Compiler" in the tree menu at the left
 1. Check the checkbox of "Enable project specific settings"
 1. In the fieldset "JDK Compliance" set "Compiler compliance level" to 7.0
 1. Click on OK
 1. If Eclipse asks you to rebuild the project, click on the Yes button.
 1. To reopen the Run Dialog, click the run button on the toolbar and click on "Open Run Dialog...":  
 ![image](img/RunToolbarButton.gif)
 1. In the Run window, Click on Java Application / Start from the tree menu at the left


Now you have TWCore compiling and runnable from within Eclipse. You are ready to develop and test your bots!

  
## Make 4 spaces on a tab
We recommend that for TWCore programming the 4 spaces be actual spaces. 

You can set Eclipse to use the tab key to make 4 spaces rather than a tab character by doing the following;

 1. Open your TWCore project in Eclipse
 1. Go to the menu Project -> Properties
 1. In the new window click on the [[+]] before "Java Code Style" and select "Formatter"
 1. Check the checkbox for "Enable project specific settings"
 1. Create a new profile by clicking the New... button and give it a name. (A built-in profile can't be changed.)
 1. If necessary, click on the Edit... button to edit the newly created Profile
 1. On the first tab (Indentation) set "Tab policy:" to "Spaces only"
 1. Verify that "Indentation size" and "Tab size" is set to 4
 1. Close the open dialog windows by click on the OK button

  
## Committing your changes
See [Contributing to TWCore: Committing/submitting your changes](Contribute).

## Final Notes
Depending on your level of experience, the first time you see TWCore's code, it might all be a bit overwhelming, confusing or simply a maze in general. It is indeed true that various parts of the code are barely documented, and that many gems are well hidden.  

To assist you with writing your own bot, or modifying an existing one, we do have several sources that can assist you:

 * [Bot Creation Tutorial](Tuturial1MakingaBasicbot);
 * [Programming guidelines](Guidelines) and a few [Tips & Tricks](TipsTricks);
 * Helpful [Developers](Developers) that you may contact in-game;
 * [TWCore's JavaDocs](http://javadoc.twcore.org).

If you think you've found a problem in our code and want to inform us of it, then please use the 'New Ticket' button at the top of the page to report the issue.