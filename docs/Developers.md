<!-- Name: Developers -->
<!-- Version: 151 -->
<!-- Last-Modified: 2014/08/24 01:24:27 -->
<!-- Author: Trancid -->

# TW Development Team
The Trench Wars' Development Team encompasses four separate units that usually work in cohorts to develop all aspects of SSCU Trench Wars. 

__TW Development Leader:__ *KrynetiX*   
# TW Bot Development Team
The TW Bot Development Team (usually shortened into Bot Dev) is responsible for and develops Trench Wars' numerous bots. This team is part of the TW Dev team, which is responsible for all of the development within SSCU Trench Wars.

The TW Bot Development Team currently consists of the following members:

__TWBotDev Leader:__ *ThePAP/Trancid*  
__TWBotDev Asisstant:__   
  
__Senior Members:__  
Senior Members are trusted Team Management/Advanced members, They are promoted to show their dedication to Bot Development.
-They have a say in what to do in an unexpected situations if the leader is offline.

 * *CRe/zreqdf*
 * *Dezmond*
 * *dugwyler/qan*
 * *fLaReD*
 * *K A N E*
 * *POiD*
 * *WingZero*


  
__Members:__  
Members have full access on twcore.org's repository and access to the TW Dev Zone. To become a full member, a Junior Member must have completed a task/assignment, checked it into the repository, and successfully closed the ticket. They must also be trusted by the Team Management.


 * *adaM*
 * *Cheese*
 * *Dral*
 * *Dexter* 
 * *Eria*
 * *Infrared*
 * *JoyRider*
 * *oO.Oo*
 * *Plenty O`Toole*
 * *Phrenitis*
 * *Rob D*
 * *Star Fox*
 * *subby*
 * *WillBy*


  
__Junior Members:__  
Junior Members are part of the bot team but are either still learning/training or haven't completed their first assignment yet.


 * *9th!*
 * *Ahmad~/red desert* 
 * *Board*
 * *Cyclone*
 * *DarkGriz*
 * *downisme*
 * *flawed soul*
 * *JabJabJab*
 * *paradise*
 * *TaxiToFlag*


  
# TW Web Development Team
The TW Web Development Team (usually shortened into Web Dev) is responsible for and develops the site @ trenchwars.org and all it's subsidiaries. This team is part of the TW Dev team, which is responsible for all of the development within SSCU Trench Wars.

The TW Web Development Team currently consists of the following members:

__TWWebDev Leader:__ *o)-<|*  
  
__Senior Members:__  
Senior Members are trusted Team Management/Advanced members, They are promoted to show their dedication to Web Development.
-They have a say in what to do in an unexpected situations if the leader is offline.

 * *POiD*

  
__Members:__  
Members have full access on twcore.org's repository and access to the TW Dev Zone. To become a full member, a Junior Member must have completed a task/assignment, checked it into the repository, and successfully closed the ticket. They must also be trusted by the Team Management.



  
__Junior Members:__  
Junior Members are part of the bot team but are either still learning/training or haven't completed their first assignment yet.


 * *Ardour*
 * *cederic+*
 * *dugwyler/qan*
 * *Weedle*
 * *K A N E*
 * *Sniperdemon*
 * *KrynetiX*


  
# TW Map and Graphics Development Team
The TW Map Development Team (usually shortened into Map Dev) is responsible for the development of various maps, events, graphics and sound found throughout Trench Wars. This team is part of the TW Dev team, which is responsible for all of the development within SSCU Trench Wars.

The TW Map Development Team currently consists of the following members:

__TWMapDev Leader:__ *true_vision*  
  
__Senior Members:__  
Senior Members are trusted Team Management/Advanced members, They are promoted to show their dedication to Map Development.
-They have a say in what to do in an unexpected situations if the leader is offline.


 * *left_eye*
 * *lego/legas*
 * *pinkstar*
 * *wiibimbo*
 * *Krynetix/Aquarius*
 * *JabJabJab*
 * *AcidBomber*
 * *F22 Raptor*
 * *Sniperdemon*


  
__Members:__  
Members have full access on twcore.org's repository and access to the TW Dev Zone. To become a full member, a Junior Member must have completed a task/assignment, checked it into the repository, and successfully closed the ticket. They must also be trusted by the Team Management.

 * *Hack*
 * *wicket666*


  
__Junior Members:__  
Junior Members are part of the team but are either still learning/training or haven't completed their first assignment yet.


 * *Shaddowknight/Sir Shaddow <ER>*
 * *Weedle*
 * *gripe*




  
# TW Data Development Team
The TW Data Development Team (usually shortened into data) is responsible for the maintenance and development of Trench War's databases. This team is part of the TW Dev team, which is responsible for all of the development within SSCU Trench Wars.

The TW Data Development Team currently consists of the following members:

__DBA/DataLeader:__ *24*  
  
__Senior Members:__  
Senior Members are trusted Team Management/Advanced members, They are promoted to show their dedication to persistence of data.
-They have a say in what to do in an unexpected situations if the leader is offline.


 * *WingZero*
 * *o)-<|*


  
__Members:__  
Members have full access on twcore.org's repository and access to the TW Dev Zone. To become a full member, a Junior Member must have completed a task/assignment, checked it into the repository, and successfully closed the ticket. They must also be trusted by the Team Management.

 * *Ardour*


  
__Junior Members:__  
Junior Members are part of the team but are either still learning/training or haven't completed their first assignment yet.


  
# Previous Developers
The following developers have worked on TWCore in the past:  
(in chronological/random order)

 * Dock> (Co-Creator)
 * Sphonk (Co-Creator)
 * 2dragons
 * lnx
 * Rodge_Rabbit
 * Cpt.Guano!
 * FoN
 * Thomas
 * script
 * Sika
 * Kyace
 * Stultus
 * Mr. Spam
 * Ikrit / Jacen Solo
 * Demystify
 * flibb
 * Mythrandir
 * Ayano
 * D1st0rt
 * milosh
 * fantus
 * Maverick
 * veLOce
 * Dezmond
 * Arobas+
 * SpookedOne

We honor each developer and contribution to TWCore.