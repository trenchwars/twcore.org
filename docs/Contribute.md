<!-- Name: Contribute -->
<!-- Version: 9 -->
<!-- Last-Modified: 2016/01/13 17:53:42 -->
<!-- Author: milosh -->


# Contributing to TWCore: Committing/submitting your changes
Have you finished your code, made it compliant to the [Programming guidelines](Guidelines) and tested it completely? Then you're ready to commit your code to the TWCore source repository so it can be reviewed!

Please keep in mind that to be able to commit your code, you will need to at least be a Junior Bot Developer. If you do not have this access, or are unsure as to what access you have, then please contact any [Developers](Developers), in-game or via email and they will direct you to the right place.

## Committing using Eclipse
From the Team Synchronizing perspective you can compare your local workspace to the online source code repository. Once compared you can commit your changes or update your local copy to the changes in the repository.

![Perspective Button](img/perspectiveButton.gif#right)

1.   As the title says, we need to switch to the Team Synchronizing perspective. Since you probably haven't opened that perspective before (and thus there isn't a perspective switcher button for it yet), use the Open Perspective button and select "Other..." to choose the Team Synchronizing perspective (see the picture at the right).  
Tip: Right-click a button of the Perspective Switcher toolbar and uncheck "Show Text" to decrease the size of the buttons and make the buttons fit better.
    ![Perspective Button](img/synchronizeButton.gif#right)
1. Click the synchronize button (see the picture at the right). (You only need to set the settings once, the next time it will synchronize immediately.)
1. In the new Synchronize window, click on the SVN option and click Next.
1. Choose what resource to synchronize: Click the Workspace radiobutton under "Scope" or only select the TWCore project.
1. Click Finish to start synchronizing.
1. If Eclipse asks you for your username/password, enter the username/password you use here too.
1. Once done synchronizing you'll see a tree with files at the left when changes are detected.
   Each file can have one of the following (or multiple combined) icons which represent a certain change:
    * ![Perspective Button](img/syncFolderChanges.gif) = (The contents of) a folder has been changed in the repository.
    * ![Perspective Button](img/syncFileChanges.gif) = (The contents of) a file has been changed in the repository.
    * ![Perspective Button](img/syncNewFile.gif) = A complete new file has been added to the repository but isn't in your workspace.
    * ![Perspective Button](img/syncLocalFileChanges.gif) = (The contents of) a file has been changed in your workspace.
    * ![Perspective Button](img/syncNewLocalFile.gif) = A complete new file has been added to your workspace but doesn't exist in the repository.
    * ![Perspective Button](img/syncFileConflicts.gif) = (The contents of) a file has been changed in the repository but also in your workspace. This is called a conflict.
    * Each of these icons can have a ![Perspective Button](img/syncWarnings.gif) or ![Perspective Button](img/syncErrors.gif) that shows if the file contains compilation errors and/or warnings.  
 1. Double-click a file to see the specific changes in the compare editor at the right.
 1. Depending on the change, do the following:
    * For files marked by the blue arrow (change in the repository), right-click and click "Update" to update your local files with the changes from the repository  
      Tip: To update multiple files at the same time, select multiple files using Ctrl or Shift, right-click and click "Update".  
      Tip2: To update all files at the same time you can right-click your project and click "Update".  
    * For files marked by the black arrow (local change), right-click and click "Commit..." to commit your local changes to the repository. Eclipse will ask you for your username/password (if necessary) and a commit message. __Always specify a commit message__ where you describe what the change contains, why you have changed it and/or referring to a specific ticket.  
    * If you have any conflicting files (marked with the red arrow: ![Perspective Button](img/syncFileConflicts.gif)) you must solve this conflict first before updating and/or committing the changes. Double-click the conflicting file and check which parts are conflicting (marked with red). Change the contents of your local file to resolve the conflict. If you have resolved the conflict but there is still a conflict showing, you can right-click the file and click "Mark as Merged". This will reset the file and any changes you still have can be committed.  
    Tip: If your local changes are neglectible you can override your local changes by choosing "Override and Update" from the right-click menu.

Note: You don't have to commit/update everything. Sometimes you'll find files that don't belong in the repository. You can have the repository ignore these files by choosing "Add to svn:ignore" from the right-click menu. However, only do this to files that should be ignored by other developers too (and you're sure they can be ignored).

If everything went well then you have committed your work successfully! You can check the [/timeline timeline] to see your changes summarized in a changeset.

### Committing "blind"
From the Java Perspective it's possible to commit your changes "blind": unaware of any other changes that were made. You can do this by clicking a changed file (marked by the ![Perspective Button](img/syncChange.gif)), right-click and choose Team / Commit... .

It's strongly recommended to commit your changes using the Synchronizing perspective (not committing "blind") since that enables you to see other changes and solve any conflicts at the same time.

## Committing using the command line
You can also forego Eclipse and commit using SVN standalone on the command line. It's also possible to use other SVN wrappers; however, we'll cover the bare minimum here.

If you used to the [RunningOnUnix guide](/RunningOnUnix) then you initially checked out the TWCore repository with `svn checkout -username <username> http://www.twcore.org/svn TWCore`. To pull down changes you can use `svn update`. It's recommended to do this before you try to push new commits to the repository so you can resolve any conflicts that may occur with an updated version.

Edit the desired files and `svn add <file>` them if they are new to TWCore. You can view files you've changed with `svn status`. You can then `svn commit -m "This is my commit message."` to commit all of your changes. Insert your twcore.org password when prompted.

Often you will want to commit changes to a few files without committing all your changes. You can do this by specifying their file paths in the commit command. (e.g. `svn commit -m "My message." src/file1.java src/file2.java`).

If you have issues using SVN or are unsure what effect your commands may have on the project then _please_ stop! Ask for help! Otherwise your commands may have serious side effects on the project.