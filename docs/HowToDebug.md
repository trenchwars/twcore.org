<!-- Name: HowToDebug -->
<!-- Version: 1 -->
<!-- Last-Modified: 2008/04/17 13:22:29 -->
<!-- Author: Maverick -->


# How to debug TWCore using Eclipse
Debugging in Eclipse is fairly easy and straightforward if you get the hang of it.

In a nutshell:

1. Set a breakpoint somewhere in the code where you want to check the variables or to see the flow of the executing code.
1. Switch to the debug perspective when the breakpoint is passed
1. Examine the variables in the debug perspective
1. Use the debug buttons (Step Into, Step Over, Step Out) to see the code that's being executed, line by line.

Notice:

* You need to start your program in debug mode to be able to debug it (else your breakpoints will be ignored).
* When debugging a TWCore bot, it can time-out if you keep it suspended for too long. This is normal as any client need to keep sending packets to the server to make sure it's still alive. If you want to prevent this, you just have to resume your thread within a timely fashion after it's suspended on a breakpoint.


## Step-by-step
As an example, I will debug the following modified (and simplified) ultrabot as I need to know the value of the variable `messagesCount` on line *18* :

```java
package twcore.bots.ultrabot;

import twcore.core.*;
import twcore.core.command.*;
import twcore.core.events.*;

public class ultrabot extends SubspaceBot {

    private int messagesCount;

    /** Creates a new instance of ultrabot */
    public ultrabot(BotAction botAction) {
        super(botAction);
    }

    public void handleEvent(Message event) {
        messagesCount++;
        if(messagesCount == 70) {
            m_botAction.die();
        }
    }

    public void handleEvent(LoggedOn event) {
        m_botAction.joinArena("#robopark");
    }
}

```



... More to come :)