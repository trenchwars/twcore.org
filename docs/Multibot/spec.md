---
disable_toc: true
---

<!-- Name: Multibot/spec -->
<!-- Version: 4 -->
<!-- Last-Modified: 2007/10/29 05:00:14 -->
<!-- Author: Mr. 420 -->
# Spec Utility 

This is a utility of [Multibot](/Bots/Multibot)

This utility allows the user 4 methods of speccing, in order of precedence:_1 is highest, 4 is lowest_

 1. *Speccing specific people*
 2. *Speccing specific ships*
 3. *Speccing specific freqs*
 4. *Speccing all playing players*  

*__Added Features__*

 * *`AddDeath <PlayerName>`* will add 1 death to PlayerName by making a new Spec Task of the player at 11 deaths.  
 * *`SpecShared <Deaths>`* specs a freq when a certain number of COMBINED deaths is reached. *_For example:*_ If this number was 30, and there were 3 people on the freq, one with 3 deaths, one with 7 deaths, and one with 19, the next time any player died, the 30 death limit would be reached and the entire freq would be sent to spec.  
 * *`SpecNotSafe`* will place any player who is not in a safe zone in spec.

*__Available commands:__*

 * *`!help spec`* Returns available commands:
    * *`!Spec <Deaths>`*                          -- Specs everyone at <Deaths> deaths.
    * *`SpecFreq <Freq>:<Deaths>`*               -- Specs <Freq> freq at <Deaths> deaths.
    * *`SpecShip <Ship>:<Deaths>`*               -- Specs <Ship> ship at <Deaths> deaths.
    * *`SpecPlayer <Player>:<Deaths>`*           -- Specs <Player> player at <Deaths> deaths.
    * *`SpecNotSafe`*                            -- Specs all players not in safe.
    * *`SpecShared <Deaths>`*                    -- Specs everyone on a freq at COMBINED <Deaths>.
    * *`AddDeath <Player>`*                      -- Specs <Player> player at one more death.
    * *`SpecList`*                               -- Shows list of all spec tasks you've entered.
    * *`SpecDel <Task Number>`*                  -- Removes spec task number <Task Number>.
    * *`SpecOff`*                                -- Stops watching deaths.
