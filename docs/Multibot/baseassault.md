---
disable_toc: true
---

<!-- Name: Multibot/baseassault -->
<!-- Version: 2 -->
<!-- Last-Modified: 2007/10/29 05:53:43 -->
<!-- Author: Mr. 420 -->
# Multibot/baseassault #
This is a game type of [Multibot](/Bots/Multibot)

To use this game type, use *`!lock baseassault`* in the arena you wish to host.

*`|                      --  General Rules --                         |`*  
*`|   Base Assault Consists of two teams--one begins with the base    |`*  
*`|   and the other tries to conquer it as quickly as possible.       |`*  
*`|   Once the base is conquered, the teams swap positions. Two       |`*  
*`|   game modes exist: a) Two rounds- the team with the fastest      |`*  
*`|   time wins. b) The game will continue until a time has not       |`*  
*`|   been beaten. The current mode is ..`*  

*__Available commands__*

* *`!help`* Returns available commands, such as:
    * *`Commands for the BA module (Author: script)`*
    * *`!start`*                     -- Begins a match of Base Assault if the settings have been confirmed.
    * *`!stop`*                      -- Stops any current Base Assault matches.
    * *`!settings`*                  -- Displays the current settings. These must be confirmed to begin Base Assault.
        * '''`!maps`'''                    -- Displays the list of maps to choose from.
        * '''`!setmap <num>`'''            -- Changes the selected map.
        * '''`!newmap name,x1,y1,x2,y2`''' -- Used to create a custom map.
        * '''`!testwarp`'''                -- Warps you to the map's associated coordiantes to ensure the settings are correct.
        * '''`!maxtime <time>`'''          -- Changes the maxtime to specified time in milliseconds.
        * '''`!mode`'''                    -- Alternates between unlimited rounds and two rounds.
        * '''`!firstdef <freq>`'''         -- Changes which freq will be first to defend.
        * '''`!firstattack <freq>`'''      -- Changes which freq will be first to attack.
        * '''`!confirm`'''                -- Used to confirm settings.

_There is a hidden *`!manual`* command which is used to toggle a few automatic hosting features on or off (default is on)_

*__Available commands to public__*

* *`!help`* Returns available commands, such as:
    * *`!status`*           -- Tells the status of the game.
    * *`!host`*             -- Tells who the current host is.
