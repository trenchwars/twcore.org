---
disable_toc: true
---

<!-- Name: Multibot/acro -->
<!-- Version: 2 -->
<!-- Last-Modified: 2007/10/29 05:52:57 -->
<!-- Author: Mr. 420 -->
# Multibot/acro 
This is a game type of [Multibot](/Bots/Multibot)

_acro_ is short for _acronym_.  

*`Rules for Acromania: compose a sentence with the letters provided.`*  
*`PM your answers to me before the timer is up!`*  
*`Then vote for your favorite answer.  You can't vote for your own!`*  
*`If you don't vote for someone else's acro, you can't win.`*  

*__Available Commands__*

* *`!help`* Returns the available commands, such as:
    * *`!start`*         -- Starts a game of Acromania.
    * *`!stop`*          -- Stops a game currently in progress.
    * *`!showanswers`*   -- Shows who has entered which answer.
    * *`NOTE: This event should only be hosted by Mod+!`*

