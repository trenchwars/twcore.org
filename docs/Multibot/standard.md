---
disable_toc: true
---

<!-- Name: Multibot/standard -->
<!-- Version: 1 -->
<!-- Last-Modified: 2007/10/29 05:52:05 -->
<!-- Author: Mr. 420 -->
# Standard Utility 
This is a utility of [Multibot](/Bots/Multibot)

*__Available Commands:__*

 * *`!help`* Returns the following commands:
    * *`!random <sizeOfFreqs>`*                      -- Randomizes players to freqs of a given size.
    * *`!teams <numberTeams>`*                       -- Makes the requested number of teams.
    * *`!door <-2 to 255>`*                          -- Changes door mode.  -2 and -1 are random/on-off modes.
    * *`!restart`*                                   -- Restarts the ball game (*restart)
    * *`!dolock`*                                    -- Locks the arena (will guarantee lock; is NOT a toggle)
    * *`!dounlock`*                                  -- Unlocks the arena (will guarantee unlock; is NOT a toggle)
    * *`!where`*                                     -- Robo will tell you his location. Remote PM only.
    * *`!setship <ship>`*                            -- Changes everyone to <ship>
    * *`!setship <freq> <ship>`*                     -- Changes everyone on <freq> to <ship>
    * *`!setfreq <freq>`*                            -- Changes everyone to <freq>
    * *`!setfreq <ship> <freq>`*'                    -- Changes everyone in <ship> to <freq> (-<ship> for 'other than')
    * *`!merge <freq1> <freq2>`*                     -- Changes everyone on <freq1> to <freq2>
    * *`!teamsspec <numTeams>`*                      -- Makes requested # of teams, specs all, & keeps freqs.
    * *`!specfreq <freq>`*                           -- Specs everyone on <freq>, but keeps them on their freq.
    * *`!specallkeepfreqs`*                          -- Specs everyone, but keeps them on their freq.
    * *`!restore`*                                   -- Setship/setfreq to prior state anyone spec'd by these
    * *`!clearinfo`*                                 -- Clear all information stored about player freqs & ships.