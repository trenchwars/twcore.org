---
disable_toc: true
---

![logo](img/twcore_logo.png#center)

## Welcome to the TWCore Project Site

This website serves as a central public information point for the Trench Wars Bot Core project
of the 2-D Multiplayer Spaceship game ​[Subspace/Continuum](http://www.getcontinuum.com/).

## About TWCore
Continuum (also known as Subspace) is a 2D online massive-multiplayer online game (MMOG). See [Wikipedia](http://en.wikipedia.org/wiki/Continuum_(game_client)) for more information. You can get Continuum from [http://www.getcontinuum.com/](http://www.getcontinuum.com/).

TWCore is the Development Team. TWCore consists of many Bot, Web, Map, and Data developers that represent the constant change to SSCU Trench Wars. This is home to our bot core written in Java for Continuum along with maps, graphics, sounds, web pages, and other resources. Started out as a small experiment by aspiring coders, TWCore has now become the stable core of Trench Wars, serving hundreds of players at the same time with a playerbase of over 10.000 players. It features mysql support and comes with about 30 prepackaged bots.
The prepackaged bots include the multibot and the famous Event Referee-assistant TWBot which can load modules for specific events or utilities.

TWCore is setup in a different way then, for example, [Mervbot](http://www.mervbot.com); TWCore spawns a hubbot in the specified zone on startup which can spawn other bots by command.