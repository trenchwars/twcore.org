<!-- Name: TipsTricks -->
<!-- Version: 4 -->
<!-- Last-Modified: 2010/02/20 11:43:49 -->
<!-- Author: dezmond -->


# Programming - Tips & Tricks
## Working with the repository / Subversion
### Changing the commit message
In Eclipse you can change the commit message if you made a mistake when comitting code;

 1. On a specific file, folder or project in Eclipse, right-click on it
 1. Choose Team / Show History
 1. A new small window is showed with the tab title 'History' showing all the revisions of the specific file(s)
 1. Right click on the revision line you want to change the message of
 1. Choose 'Set Commit Properties'
 1. Change the message in the new popup window and click on OK

Note: Trac / TWCore.org won't show your new commit message as it catches these messages in its database. When a repository refresh is done, these messages are updated.


## Working with Eclipse / Java programming
### Use the Eclipse shortcut key combinations!
It's possible to dedicate many pages to the shortcut key combinations of Eclipse. You can see all the possible shortcut keys for your current view with *Ctrl + Shift + L*. Here are some shortcut keys I use frequently or are quite handy:

 * *Ctrl + Shift + R*  
   Open resource. If you quickly want to open a specific file, for example HubBot.java, use this shortcut key and begin typing 'HubBot'. The file will be found quite rapidly and you only need to press <enter> to open it.

 * *Ctrl + Shift + T*  
   Open type. Same as above but with Open Type you can quickly open any class or java object.

 * *Ctrl + Shift + O*  
   Organize imports. Adds missing, cleans unused and organizes imports for your current Java class. It can be used for solving one of the following situations; if you have a warning in your class file about an unused import and/or if you have an error about an unresolved class.

 * *Alt + Shift + J*  
   Add Javadoc. If you have a method without javadoc, use this shortcut key to quickly add a javadoc (template).

 * *Ctrl + H*  
   Open Search window. Usually you only use the File Search tab as it can be used to quickly search for text in files.

 * *Ctrl + Shift + G*  
   Select a method or object and use this shortcut key to quickly find all references to this method or object (all code that uses this method or object).

To run the bots in Eclipse, you don't have to keep right-clicking the start.java and going to Run as. Just highlight the start.java and on the top bar of your environment, Click the play button (run) which will start the core :).

Do you have more handy or useful shortcut keys that you want to share? Feel free to add them to this list.