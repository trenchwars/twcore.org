<!-- Name: Guidelines -->
<!-- Version: 12 -->
<!-- Last-Modified: 2009/05/28 12:10:11 -->
<!-- Author: Maverick -->

# Programming guidelines
For programming on TWCore you should try to follow the [Sun Code Conventions](http://java.sun.com/docs/codeconv/) as much as you can. 

However, there are a few exceptions:

* The Sun code conventions prescribe making lines not longer than 80 characters. Instead it's recommended to not make lines longer than 120 characters.

* Put opening braces `{` on the same line as the statement, rather than on a new line. Closing braces `}` may be on a new line.

* Sun conventions specify that indentation should be 4 spaces, but not whether this should be done through tab-setting or an actual 4 spaces. We recommend that for TWCore programming the 4 spaces be actual spaces. See the [Programming guide](Programming#Make4spacesonatab) on how to modify the Eclipse settings to do this automatically.   

Other highlights:

* __Always__ put JavaDoc comments before methods and public class variables. This way we get automatic documentation of TWCore through the [/javadoc JavaDoc] and everything will be more clearer for other developers.  If you aren't familiar with JavaDoc conventions, take a look at other bots in TWCore, or read over the [official JavaDoc tutorial](http://java.sun.com/j2se/javadoc/writingdoccomments/) here.

* We're using Java 6! Use it to its full potential! (Don't use any deprecated methods.)

* Try to keep things as clean as possible. Examples:
    * [Keep It Simple Stupid](http://en.wikipedia.org/wiki/KISS_principle)! Try to keep your code as short and simple as possible. Don't make overly complex code or too much code when you can do it shorter and simpler.
    * Don't have to much repetitive code in your class; instead put it in a separate method.  If you find you repeat 5 lines or more, chances are it should be in its own method.
    * Don't create redundant or obfuscated code; do it smart, not quick and ugly.  Think of the coder that will have to maintain and update your code after you (it will happen) -- 90% of a programmer's time is spent maintaining and fixing careless code.
    * Try to create bots Object Oriented. Don't force your bot to be overly Object Oriented if it creates a lot of redundant code, messes things up, or makes it impossible for anyone else to understand.  There are actually bots in TWCore that can't be used or updated because the coder made them too OO, left no good diagram or documentation, and consequently made it impossible for anyone else to understand.
    * When your bots does things that the BotAction.java already has a method for (like `?arena` or a `double*spec`) use BotAction.java's method instead of doing a raw command yourself!

* If you think you can do better, don't hesitate to show it! 

* When committing, always specify a commit message so other people know what you did. Make sure the message is as specific as possible as it helps quite a bit in the long run. Note that the message is also displayed on the [/timeline timeline].

Do you have any other guidelines/highlights? Feel free to add them.