@ECHO OFF
ECHO ==== TWCORE UPDATE ====
IF NOT EXIST twcore mkdir twcore
cd twcore

ECHO Exporting TWCore...
call ..\bin\svnkit\jsvn.bat export http://www.twcore.org/svn/trunk/twcore/src/ .\src_new
ECHO Moving files...
IF EXIST src rmdir /S /Q src
move .\src_new .\src

ECHO Exporting library files...
call ..\bin\svnkit\jsvn.bat export http://www.twcore.org/svn/trunk/twcore/lib/ .\lib_new
ECHO Moving files...
IF EXIST lib rmdir /S /Q lib
move .\lib_new .\lib

IF EXIST build.xml del build.xml
ECHO Exporting build.xml ...
call ..\bin\svnkit\jsvn.bat export http://www.twcore.org/svn/trunk/twcore/build.xml build.xml

ECHO All done!
pause