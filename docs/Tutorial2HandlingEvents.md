---
disable_toc: true
---
<!-- Name: Tutorial2HandlingEvents -->
<!-- Version: 2 -->
<!-- Last-Modified: 2007/07/31 15:58:53 -->
<!-- Author: Maverick -->

(This tutorial has been copied with the approval of D1st0rt from [D1st0rt's website](http://d1st0rt.sscentral.com).)

# Tutorial 2: Handling Events
_Written by D1st0rt @ Friday, 19 November 2004_  
_Last updated: Monday, 12 June 2006_

So you've got a bot, now you need it to do something. For this, the twcore comes with an EventRequester. If you want your bot to do something when an event happens, you need to request the event and then handle the event. Lets make our bot welcome players that come in.

```java
//Make the package name the same as the bot's name so you can spawn it
package twcore.bots.mybot;
//Import all of the TWCore classes so you can use them
import twcore.core.*;

public class mybot extends SubspaceBot
{
    //Requests all of the events from the core
    private EventRequester events;

    //Creates a new mybot
    public mybot(BotAction botAction)
    {
        //This instantiates your BotAction
        super(botAction);
        //Instantiate your EventRequester
        events = m_botAction.getEventRequester();
        //Request PlayerEntered events
        events.request(EventRequester.PLAYER_ENTERED);
    }

    //What to do when the bot logs on
    public void handleEvent(LoggedOn event)
    {
        //Get the data from mybot.cfg
        BotSettings config = m_botAction.getBotSettings();
        //Get the initial arena from config and enter it
        String initial = config.getString("InitialArena");
        m_botAction.joinArena(initial);
        //NOTE: m_botAction is inherited from SubspaceBot
    }

    //What to do when a player enters the arena
    public void handleEvent(PlayerEntered event)
    {
        //Get the name of player that just entered
        String name = event.getPlayerName();
        //Greet them
        m_botAction.sendPrivateMessage(name,"Welcome!");
    }
}
```

Now, every time a player enters the arena, they will be welcomed by the bot. There is lots of information that comes with events, make sure you check the docs to see what you can do. Here is a list of the events you can request:

* Chat messages sent (MESSAGE)
* Player entered (PLAYER_ENTERED)
* Bot gets the list of arenas (ARENA_LIST)
* Player moves (PLAYER_POSITION)
* Player leaves (PLAYER_LEFT)
* Player dies (PLAYER_DEATH)
* Player gets a prize (PRIZE)
* Player score changes (SCORE_UPDATE)
* Player uses a weapon/item (WEAPON_FIRED)
* Player changes frequency (FREQUENCY_CHANGE)
* Player changes ship (FREQUENCY_SHIP_CHANGE)
* Bot logs on (Don't need to request it) (LOGGED_ON)
* A file arrives (FILE_ARRIVED)
* Bot joins an arena (ARENA_JOINED)
* Flag victory occurs (FLAG_VICTORY)
* Periodic flag reward occurs (FLAG_REWARD)
* Scores are reset (SCORE_RESET)
* Watchdamage packet received (WATCH_DAMAGE)
* Goal scored (SOCCER_GOAL)
* Ball moves (BALL_POSITION)
* Flag moves (FLAG_POSITION)
* Flag dropped (FLAG_DROPPED)
* Flag claimed (FLAG_CLAIMED)
* Player attaches to (or detaches from) another (TURRET_EVENT)


[Previous Tutorial](/Tuturial1MakingaBasicbot) | [Next Tutorial](/Tutorial3Commands)