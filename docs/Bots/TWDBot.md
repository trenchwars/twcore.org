---
disable_toc: true
---
<!-- Name: TWDBot -->
<!-- Version: 5 -->
<!-- Last-Modified: 2008/02/27 23:39:25 -->
<!-- Author: Maverick -->
# TWDBot
## Database usage
|  *Command*                 |  *Table*                  |  *Currently set to database*  |
| --- | --- | --- |
|  !add                          |  tblTWDPlayerMID              |  local                            |
|  !removemid                    |  tblTWDPlayerMID              |  local                            |
|  !removeip                     |  tblTWDPlayerMID              |  local                            |
|  !removeipmid                  |  tblTWDPlayerMID              |  local                            |
|  !listipmid                    |  (DBPlayerData)[[BR]]tblTWDPlayerMID  |  local                    |
|  !signup                       |  (DBPlayerData)               |  local                            |
|  !squadsignup                  |  (DBPlayerData) (Check if registered and disabled)[[BR]]tblTeam[[BR]]tblTeamUser  |  local[[BR]]website[[BR]]website  |
|  !registered                   |  (DBPlayerData)               |  local                            |
|  !resetname[[BR]][[BR]]--> resetRegistration  |  (DBPlayerData)[[BR]]tblTWDPlayerMID[[BR]]tblAliasSupression  |  local[[BR]]website[[BR]]local  |
|  !cancelreset                  |  tblAliasSupression           |  local                            |
|  !resettime                    |  tblAliasSupression           |  local                            |
|  !enablename                   |  (DBPlayerData)               |  local                            |
|  !disablename                  |  (DBPlayerData)               |  local                            |
|  !info                         |  (DBPlayerData)               |  local                            |
|  !register                     |  (DBPlayerData)               |  local                            |
|  !altip                        |  tblAliasSupression, tblUser  |  local                            |
|  !altmid                       |  tblAliasSupression, tblUser  |  local                            |
|  (Periodic) checkNamesToReset  |  tblAliasSupression           |  local                            |
|  (Periodic) checkMessages      |  tblMessage                   |  website                          |

Database website = trenchwars.org  
Database local = RoboQueen