---
disable_toc: true
---

<!-- Name: HubBot -->
<!-- Version: 2 -->
<!-- Last-Modified: 2009/09/08 09:04:55 -->
<!-- Author: Maverick -->
# HubBot
The HubBot is the main bot of TWCore. It is responsible for spawning new bots, shutting down bots and access control. HubBot also features a few commands for status information and troubleshooting.
HubBot is always spawned automatically whenever TWCore is started as it's the first bot in the zone to represent TWCore.

## Help interface
The current !help interface for owner access looks like this:

```
     TWHub> Access: Owner [lvl 9]
     TWHub> You have access to 19 / 19 commands.
     TWHub> -----------------------------------------------
     TWHub> BOT CONTROL:      !spawn !spawnmax !spawnauto !forcespawn !waitinglist !listbots
     TWHub>                   !remove !removetype
     TWHub>                   !shutdowncore !smartshutdown !shutdownidlebots !shutdownallbots
     TWHub> ACCESS CONTROL:   !updateaccess !listoperators
     TWHub> SERVER TROUBLESH: !billerdown !recycleserver
     TWHub> STATUS:           !uptime !dbstatus !version
     TWHub> -----------------------------------------------
     TWHub> Message ::!help <command> for more information.
```

You can private message `!help <command>` for more information. This command information explains what the command does, the syntax, the arguments and the access required to execute this command.