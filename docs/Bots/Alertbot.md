---
disable_toc: true
---
<!-- Name: Alertbot -->
<!-- Version: 1 -->
<!-- Last-Modified: 2008/04/01 10:56:58 -->
<!-- Author: Maverick -->
# Alertbot
Alertbot is used to notify subscribers when an event is (re)starting by PM.  
It acts upon arena messages that match a predefined regular expression from the configuration file.

*base*  

| start: | `^(?:Basing|Time Race|Point Race to d+): .+ vs. .+$` |
|---|---|
|  message:   |  `A game of basing is about to begin at ?go $arena`  |
|  end:       |  `^Result of .+ vs. .+: [[d:]]+ - [[d:]]+$`  |
|  message:   |  `A basing game just ended at ?go $arena. Another game will start when enough people are here`  |


*wbduel*  

|  start:     |  `^Warbird dueling: .+ vs. .+$`  |
| --- | --- |
|  message:   |  `A game of wbduel is about to begin at ?go $arena`  |
|  end:       |  `^Result of .+ vs. .+: d+ - d+$`  |
|  message:   |  `WBduel just ended at ?go $arena. Another game will start when enough people are here`  |


*javduel*  

|  start:     |  `^Javelin Dueling: .+ vs. .+$`  |
| --- | --- |
|  message:   |  `A game of javduel is about to begin at ?go $arena`  |
|  end:       |  `^Result of .+ vs. .+: d+ - d+$`  |
|  message:   |  `Javduel just ended at ?go $arena. Another game will start when enough people are here`  |


*elim*  

|  start:     |  `^GAME OVER: (?:No winner|Winner .+)$`  |
| --- | --- |
|  message:   |  `Next $arena game is starting now!`  |
|  end:       |  `^((?:(?:Warbird|Spider|Any ship|Leviathan|Lancaster|Fighter) [[eE]]lim|Any ship race) to d{1,2})s*$`  |
|  message:   |  `$arena voted: $1`  |


*baseelim*  

|  start:     |  `^GAME OVER: (?:No winner|Winner .+)$`  |
| --- | --- |
|  message:   |  `Next $arena game is starting now!`  |
|  end:       |  `^((?:(?:Javelin|Terrier|Shark) elim in base to|Cloaker race in flagroom) d{1,2})s*$`  |
|  message:   |  `$arena voted: $1`  |


*spidduel*  

|  start:     |  `^Spider Dueling: .+ vs. .+$`  |
| --- | --- |
|  message:   |  `A game of spidduel is about to begin at ?go $arena`  |
|  end:       |  `^Result of .+ vs. .+: d+ - d+$`  |
|  message:   |  `Spidduel just ended at ?go $arena. Another game will start when enough people are here`  |
