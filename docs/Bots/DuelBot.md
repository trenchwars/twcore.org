---
disable_toc: true
---
<!-- Name: DuelBot -->
<!-- Version: 1 -->
<!-- Last-Modified: 2008/03/10 10:58:43 -->
<!-- Author: Maverick -->
# DuelBot
## Database usage
|  Command / method    |  Table                                                |  Currently set to database connection  |
| --- | --- | --- |
|  !setrules           |  tblDuelPlayer                                        |  local  |
|  !tchallenge         |  tblDuelTournyGame[[BR]]tblDuelTourny                 |  local  |
|  !enable             |  tblDuelPlayer                                        |  local  |
|  !rank               |  tblDuelLeague                                        |  local  |
|  !alias              |  tblDuelPlayer                                        |  local  |
|  do_addPlayer()      |  tblDuelPlayer[[BR]](DBPlayerData)                    |  local  |
|  endDuel()           |  (DBPlayerData)[[BR]]tblDuelMatch[[BR]]tblDuelLeague[[BR]]tblDuelPlayer   |  local  |
|  updateInactives()   |  tblDuelPlayer[[BR]]tblDuelLeague[[BR]]               |  local  |
|  tournyTalk()        |  tblDuelTournyGame[[BR]]tblDuelTourny                 |  local  |
|  checkForfeits()     |  tblDuelTournyGame[[BR]]tblDuelTourny                 |  local  |
|  sql_getPlayer()     |  tblDuelPlayer                                        |  local  |
|  sql_createLeagueData()  |  tblDuelLeague                                    |  local  |
|  sql_banned()        |  tblDuelBan                                           |  local  |
|  sql_banPlayer()     |  tblDuelBan                                           |  local  |
|  sql_bannedPlayers() |  tblDuelBan                                           |  local  |
|  sql_recommentBan()  |  tblDuelBan                                           |  local  |
|  sql_getComment()    |  tblDuelBan                                           |  local  |
|  sql_unbanPlayer()   |  tblDuelBan                                           |  local  |
|  sql_getUserInfo()   |  tblDuelBan                                           |  local  |
|  sql_storeUserLoseRating()  |  tblDuelLeague                                 |  local  |
|  sql_storeUserWinRating()  |  tblDuelLeague                                  |  local  |
|  sql_verifyRecord()  |  tblDuelLeague                                        |  local  |
|  sql_enabledUser()   |  tblDuelPlayer                                        |  local  |
|  sql_enableUser()    |  (DBPlayerData)[[BR]]tblDuelPlayer                    |  local  |
|  sql_disableUser()   |  (DBPlayerData)[[BR]]tblDuelPlayer[[BR]](DBPlayerData)[[BR]]tblDuelLeague |  local  |
|  sql_getUserIPMID()  |  tblDuelPlayer                                        |  local  |
|  sql_gameLimitReached()  |  tblDuelMatch                                     |  local  |
|  sql_getName()       |  tblUser                                              |  local  |
|  sql_updateTournyAvailability()  |  tblDuelTournyGame                        |  local  |
|  sql_updateTournyMatchData() |  tblDuelTournyGame                            |  local  |
|  sql_getTournyGameID()  |  tblDuelTournyGame[[BR]]tblDuelTourny              |  local  |
|  sql_lagInfo()       |  tblDuelPlayer                                        |  local  |
|  updatePlayoffBracket()  |  tblDuelTournyGame[[BR]]tblDuelTourny[[BR]]tblMessageSystem  |  local  |
|  advancePlayer()     |  tblDuelTournyGame[[BR]]tblDuelPlayer[[BR]]tblMessageSystem  |  local  |

Cumulative DuelBot's tables:

 * DBPlayerData
 * tblDuelBan
 * tblDuelLeague
 * tblDuelMatch
 * tblDuelPlayer
 * tblDuelTourny
 * tblDuelTournyGame
 * tblMessageSystem
 * tblUser