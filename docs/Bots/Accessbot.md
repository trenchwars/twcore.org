---
disable_toc: true
---
<!-- Name: Accessbot -->
<!-- Version: 1 -->
<!-- Last-Modified: 2007/05/04 09:45:33 -->
<!-- Author: Maverick -->
## Accessbot
This bot is used for updating staff access to the database of trenchwars.org.  
_The current dean of staff, Kim, reports that this bot isn't used for updating access anymore. It remains the question if this bot should still be used for updating access._

__Available commands:__  

 * Super Moderator+:
    * `!help` Returns available commands
    * `!update` Updates the database after checking if there is a proper connection available.
    * `!die` Disconnects the bot
 * Moderator- Bot returns a message stating that the operator doesn't have the proper access.

__Dependencies__:   

 * SQL Database with the table tblUser
 * Bot operator list

__Used staff commands:__   
_None_