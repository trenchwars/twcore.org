---
disable_toc: true
---

<!-- Name: Multibot -->
<!-- Version: 11 -->
<!-- Last-Modified: 2007/10/29 05:54:32 -->
<!-- Author: Mr. 420 -->

## Multibot
This bot is used for single-event hosting and event utility management.  Designed as a replacement for TWBot.  

*__Acessed by:__*  
  * Only *<ER>+* can use this bot.

*__Available commands:__*  
 *This is the list of commands from an _unlocked_ [Multibot](Multibot).*

 * *`!help`* Returns available commands, such as:
    * *`!Go <ArenaName>`*          --  Sends the bot to <ArenaName>.
    * *`!Follow`*                  --  Turns follow mode on.
    * *`ListGames`*               --  Lists the games that are available in this arena.
    * *`ListUtils`*               --  Lists utility modules (formerly TWBot modules).
    * *`!lock <Game>`*             --  Locks the bot and loads game <Game>.
    * *`!Load/!Unload <Utility>`*  --  Loads or unloads utility <Utility>.
    * *`!Load`*                    --  Loads standard, warp and spec modules.
    * *`UnloadAll`*               --  Unloads all utilities.
    * *`!Loaded`*                  --  Shows currently loaded utilities.
    * *`!Help <Utility>`*          --  Shows help for utility <Utility>.
    * *`!Home`*                    --  Returns the bot to its home.
    * *`!Die`*                     --  Logs the bot off.

*This is the list of commands from a _locked_ [Multibot](Multibot).*

 * *`!help`* Returns available commands, such as:
    *  *`MultiBot Help:`*             --  USE !MODHELP for help on the currently loaded module.
    * *`!Module`*                  --  Displays the module and version that is currently loaded.
    * *`!Unlock`*                  --  Unlocks the bot, which unloads the currently-loaded module.
    * *`ListUtils`*               --  Lists utility modules (formerly TWBot modules)
    * *`!Load/!Unload <Utility>`*  --  Loads/unloads utility <Utility>.
    * *`UnloadAll`*               --  Unloads all utilities.
    * *`!Loaded`*                  --  Shows currently loaded utilities.
    * *`!Help <Utility>`*          --  Shows help for utility <Utility>.
    * *`!Die`*                     --  Logs the bot off.
    * *`ModHelp`*                 --  Displays help message for currently-loaded module.

*Here is the list of all _Games_ [Multibot](Multibot) has to offer:*  

* Using the *`ListGames`* command will retrieve this list of currently supported Game types.

| [acro](/Multibot/acro)       | ballspec        | [baseassault](/Multibot/baseassault) |
|---|---|---|
| bbj         | bfallout        | bountyhunter |
| bship       | cnr             | commander |
| conquer     | dangerous       | enigma |
| fallout     | freezetag       | gangwars |
| golden      | gravbomber      | hunt |
| killer      | killrace        | marco |
| payback     | platoon         | poker |
| prodem      | rabbit          | raceelim |
| revenge     | rps             | soccerrace |
| spaceball   | speed           | starcon |
| trivia      | tugawar         | untouchable |
| zombies     |                 |

*Here is a list of all the _Utilities_ [Multibot](Multibot) has to offer:*  

* Using the *`ListUtils`* command will retrieve this list of currently supported utilities.

| antispawn   | art             | autopilot |
|---|---|---|
| donations   | doors           | etc |
| flags       | flagwarppt      | hotspots |
| lagout      | messages        | objons |
| poll        | prizes          | random |
| remote      | safes           | shipc |
| shiplimit   | shiprestrict    | spawnpmer |
| [spec](/Multibot/spec)    | spec2           | [standard](/Multibot/standard) |
| streak      | turret          | warp |
| warprand    | watchtk         |

*__How to use:__*  

* Find an available RoboBot using the command *`:#Robobots:!where`*  
  All RoboBots online will respond if they are in use or not and where they are sitting.   
* Use *`!go <arena>`* to move the bot to the specified [arena].
* If you are hosting a *Game*, use *`!lock <Game>`*.  
* If you are going to use a *Utility*, use *`!load <Utility>`*.  
* For a list of commands while a *Game* is loaded, use *`!modhelp`*.  
* For a list of commands while a *Utility* is loaded, use *`!help <utility>`*.

_For more information on a specific *Game* or *Utility*, please go it's corresponding page by clicking the name in the above lists. *(Links will appear as soon as those specific pages are updated).*_