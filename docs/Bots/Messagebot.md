---
disable_toc: true
---

<!-- Name: Messagebot -->
<!-- Version: 6 -->
<!-- Last-Modified: 2007/10/31 00:31:34 -->
<!-- Author: Mr. 420 -->
# Messagebot #
This bot is a host for _channels_ that allow a player to ?message or PM everyone on the channel so that information can be spread easily.  

## __Accessed by:__

 * *<ZH>+'s* can create channels.
 * Some commands my only be done by *SMod+* and by *Channel owners/operators*.

## __Available commands:__

* *SMod+* commands:  
    * `!help smod`*
    Returns the following commands:
     * `!addnews <news>:<url>`          -- Adds a news article with <news> as the content and <url> for more info.
     * `!delnews <#>`                   -- Deletes news ID Number <#>.
     * `!go <arena>`                    -- Sends messagebot to <arena>.
     * `!die`                           -- Kills messagebot.

 * *ZH+* commands:  
    * *`!help`*                            -- PM me with !help channel for channel system help, !help message for  message system help.  !help news for news system help, !help smod for SMod command help.
    * *`!help channel`* Returns the following commands:
        * `!me`                            -- Tells you what channels you have joined.
        * `!join <channel>`                -- Puts in request to join <channel>.
        * `!quit <channel>`                -- Removes you from <channel>.
        * `!owner <channel>`               -- Tells you who owns <channel>.
        * `!announce <channel>:<message>`  -- Sends everyone on <channel> a PM of <message>.
        * `!message <channel>:<message>`   -- Leaves a message for everyone on <channel>.
        * `!requests <channel>`            -- PM's you with all the requests to join <channel>.
        * `!banned <channel>`              -- Lists players banned on <channel>.
        * `!members <channel>`             -- Lists all members on <channel>.
        * `!ban <channel>:<name>`          -- Bans <name> from <channel>.
        * `!unban <channel>:<name>`        -- Lifts <name>'s ban from <channel>.
        * `!makeop <channel>:<name>`       -- Makes <name> an operator in <channel>.
        * `!deop <channel>:<name>`         -- Revokes <name>'s operator status in <channel>.
        * `!grant <channel>:<name>`        -- Grants ownership of <channel> to <name>.
        * `!private <channel>`             -- Makes <channel> a request based channel.
        * `!public <channel>`              -- Makes <channel> open to everyone.
        * `!destroy <channel>`             -- Destroys <channel>.
        * `!accept <channel>:<name>`       -- Accepts <name>'s request to join <channel>.
        * `!decline <channel>:<name>`      -- Declines <name>'s request to join <channel>.
        * `!create <channel>`              -- Creates a channel with name <channel>.  

    * `!help message` Returns the following commands:
        * `!messages`                      -- PM's you all your message numbers.
        * `!read`                          -- Reads all unread messages.
        * `!read <num>`                    -- Reads you message <num>.
        * `!read r`                        -- Reads all old/read messages.
        * `!read a`                        -- Reals all unread & read messages.
        * `!read #<channel>`               -- Reads all unread messages on <channel>.
        * `!read #<channel>:r`                 -- Reads all old/read messages on <channel>.
        * `!read #<channel>:a`             -- Reads all messages on <channel>.
        * `!unread <num>`                  -- Sets message <num> as unread.
        * `!delete <num>`                  -- Deletes message <num>.
        * `!delete read`                   -- Deletes messages already read.
        * `!delete all`                    -- Deletes all messages.
        * `!lmessage <name>:<message>`     -- Leaves <message> for <name>.

    * `!help news` Returns the following commands:
        * `!readnews <#>`                  -- PM's you news article #<#>.
        * `!listnews`                      -- PM's you all the news article numbers.
