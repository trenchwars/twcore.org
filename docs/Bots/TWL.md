---
disable_toc: true
---

<!-- Name: TWL -->
<!-- Version: 2 -->
<!-- Last-Modified: 2008/03/27 21:25:13 -->
<!-- Author: milosh -->
# TWL
## Database Usage
| Command / method                   | Table         | Currently set to database |
| ---- | ---- | ----- |
| MatchGame.getTeamID()              | tblTeam       | local |
| MatchGame.createGameRecord()       | tblMatch      | local |
| MatchGame.storeGameResult()        | tblMatch      | local |
| MatchLogger.createLogRecord()      | tblMatchLog   | local |
| MatchPlayer.storePlayerResult()    | tblMatchRoundUser[[BR]]tblMatchRoundUserShip | local |
| MatchRound.storeRoundResult()      | tblMatchRound | local |
| MatchTeam.populateCaptainList()    | tblUser[[BR]]tblTeamUser[[BR]]tblUserRank | local |