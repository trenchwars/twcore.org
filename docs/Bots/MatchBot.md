---
disable_toc: true
---
<!-- Name: MatchBot -->
<!-- Version: 2 -->
<!-- Last-Modified: 2008/02/27 11:43:54 -->
<!-- Author: Maverick -->
# MatchBot
## Database Usage
| Command / method                   | Table         | Currently set to database |
| ------ | ----- | ----- |
| !challenge                         | tblTeam       | local |
| MatchGame.getTeamID()              | tblTeam       | local |
| MatchGame.createGameRecord()       | tblMatch      | local |
| MatchGame.storeGameResult()        | tblMatch      | local |
| MatchLogger.createLogRecord()      | tblMatchLog   | local |
| MatchPlayer.storePlayerResult()    | tblMatchRoundUser tblMatchRoundUserShip | local |
| MatchRound.storeRoundResult()      | tblMatchRound | local |
| MatchTeam.populateCaptainList()    | tblUser tblTeamUser tblUserRank | local |
| MatchTeam.checkPlayerIPMID()       | tblAliasSuppression tblUser tblTWDPlayerMID | local |