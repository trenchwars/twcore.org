---
disable_toc: true
---
<!-- Name: Tutorial4ConfigurationFilesandSpawning -->
<!-- Version: 1 -->
<!-- Last-Modified: 2007/07/31 16:06:12 -->
<!-- Author: Maverick -->

(This tutorial has been copied with the approval of D1st0rt from [D1st0rt's website](http://d1st0rt.sscentral.com).)

# Tutorial 4: Configuration Files and Spawning
_Written by D1st0rt @ Saturday, 20 November 2004_  
_Last updated: Saturday, 20 November 2004 _

Now we need to compile our bot. Open a command prompt in your twcore folder and type `bld mybot`. (Note that compilation goes automatic using Eclipse.)

Once its compiled, we need to add a configuration file so it knows what to do. In the first section, we made a call `String initial = config.getString("InitialArena");`, which loaded data from a cfg file. Make a new text file in your mybot folder called mybot.cfg:

```java
//This is how many mybots you can have running at any one time
//make sure each has a name and password
Max Bots=2
//This is where all of your bots will go when they are spawned
InitialArena=mybotland

//The info for the first bot
Name1=MyBot
Password1=pass
//The info for the second bot
Name2=MyBot2
Password2=pass2
```

An important thing to note here is that the TWCore does not create these accounts for you, in order for the bot to login the username and password must already exist. (The newest TWCore version can create accounts.) 

We've got a configuration and a compiled bot, its time to run it! In your twcore folder, you can either double click on runbots or run it from a command prompt. When your hub bot appears in #robopark, pm it with `!spawn mybot`, and one should appear in mybotland. When you enter mybotland, the friendly mybot will welcome you.

[Previous Tutorial](/Tutorial3Commands) | [Next Tutorial](/Tutorial5TWBotExtensionBasics)