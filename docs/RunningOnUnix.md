<!-- Name: RunningOnUnix -->
<!-- Version: 4 -->
<!-- Last-Modified: 2016/01/13 18:00:36 -->
<!-- Author: milosh -->



# Running TWCore on Unix (Mac/Linux/etc)
This guide describes how to setup TWCore to run in your zone.  
If you want to develop bots for TWCore, see the [Programming](/Programming) guide.

The following steps are described:

  1. [Prerequisites](#prerequisites)
  1. [Downloading TWCore](#downloading-twcore)
  1. [Building TWCore](#building-twcore)
  1. [Configuration](#configuration)
  1. [Ready to run](#ready-to-run)

Problems while following this guide? Ask your question at the forums!

## Prerequisites
There are some things you need to have for running TWCore:

 1. Download a [Java Development Kit (JDK)](http://java.sun.com/javase/downloads/index.jsp).  
     * If you are on a Linux distribution, download and install the appropriate package. This can usually be done through your distributions package manager.  
     * If you are OS X then the JDK comes preinstalled and can likely be found at `/System/Library/Java/JavaVirtualMachines`; however, you can install a different version if desired.  
 2. [Ant](http://ant.apache.org/bindownload.cgi) (Download _Current Release of Ant_, .zip archive)  
     This may already be installed for your distribution. Try `ant -version` to check. Ant is a build tool commonly used in Java projects to compile source in an organized fashion. It operates on the `build.xml` file included in the project.
  
 3. [Subversion (SVN)](http://subversion.apache.org/packages.html)  
     This also may already be installed for your distribution. Try `svn --version` to check. SVN is version control software that allows for comparing code to previous or different versions.
  
 4. (Access to) a [Subspace zone](http://www.shanky.com/server). The easiest method is to host a zone locally for development.

    *Optional*:

 5. (Access to) a [MySQL](http://www.mysql.org/) database.

### Setting JAVA_HOME
Once you have your JDK downloaded and extracted you may need to point your $JAVA_HOME towards it.  

 1. Open up the file that declares your systems PATH variable. This will differ depending on your distribution but it might be /etc/bashrc, /etc/profile, ~/.bashrc or ~/.profile. It will look something like: 
`export PATH=/usr/local/mysql/bin:/opt/local/bin:/opt/local/sbin:$PATH`
 1. Add a line above the export PATH line and type `export JAVA_HOME='/path/to/jdk'` _(The correct directory should contain a folder called bin with the java executable in it. Path to this executable would look like `/path/to/jdk/bin/java`, but don't include the `/bin/java` on the end when declaring `JAVA_HOME`.)_
 1. Save and close the file with the `export JAVA_HOME` in it. You'll need to then close and reopen your terminal or source the file. You can check to make sure you properly declared your `JAVA_HOME` with `echo $JAVA_HOME`, which should return `/path/to/jdk`.

## Downloading TWCore
In this step you will export the most recent TWCore version from the repository of twcore.org .

 1. Navigate to the directory where you want the TWCore project to exist. (i.e. `cd ~`)
 1. Type `svn checkout -username <username> http://www.twcore.org/svn TWCore` where `<username>` is your twcore.org username.
 1. When it prompts you for a password, use your twcore.org password.

You can always update TWCore safely as it doesn't affect any running / binary files nor your configuration files.

## Building TWCore
Before you can use and configure TWCore, it needs to be built.

 1. Navigate to TWCore/trunk/twcore, where you should see a file called build.xml.
 1. Type `ant -v` with no additional parameters. This will build the project.

Note for building TWCore, TWCore must be turned off completely! Your configuration files will not be touched during the build process.

## Configuration
You must now configure TWCore so that it knows where your development zone is and what names you would like the bots to have. The configuration files include  `TWCore/trunk/twcore/bin/setup.cfg` and various files in the `TWCore/trunk/twcore/bin/corecfg` directory. You can copy example files into the `bin/` directory using `ant configCopy` in the `TWCore/trunk/twcore` directory.

 1. Rename `bin/setup.example.cfg` to `bin/setup.cfg`
 1. Open `bin/setup.cfg` and fill in the settings:
    * `[[Server]]`
        * `Server`  
        should be set to the IP of the Subspace server you are connecting to
        * `Port`  
        should be set to the port of the Subspace server you are connecting to
    * `[Login Information]`
        * `Main Login`  
        the name of the account for the TWCore Hubbot (e.g. MyHub)
        * `Main Password`  
        the password of the Continuum account for the TWCore Hubbot.
        * `Sysop Password`  
        the sysop password for the zone you are connecting to.
    * (Optional) `[Chat Names]`
        * `Chat Name`  
        the default chat name of any bots that require it. Usually this is used to set to a chat used by the TWCore developers.
        * `Staff Chat`  
        the chat name of the staff chat.
        * `Smod Chat`  
        the chat of the smod/upper staff chat.
    * (Optional) `[[Debugging]]`
        * `DebugOutput`  
        Set DebugOutput to 0 to turn stack traces off, 1 to turn them on. Stack traces are useful for debugging bots.
    * `[[Directories]]`
        * `Core Location`  
        The complete path to your core (where `build.xml` exists). Do not end this with a slash. For example: `C:\Subspace\TWCore` (Windows) or `/home/bots/twcore` (Linux).
    * (Optional) `[[Logging]]`
        * `Exception Log`  
        Set the file and path of the exception log. This log will contain any exceptions the bot runs into. For example: `C:\Subspace\TWCore\exception.log` or `/home/bots/twcore/exception.log`
        * `LogPrivateMessages`  
        Set LogPrivateMessages to 1 to have each bot record private messages in the /logs folder.

 3. Rename all the example files in the `bin/corecfg` directory to .cfg. For example: `autoload.example.cfg` to `autoload.cfg`. Here is a short description of each configuration file:
    * (Optional) `autoload.cfg`  
        * This is a list of bot types. All the bot types in this file will be automatically spawned when TWCore is started and connected to the Subspace server.
    * `operators.cfg`  
        * This file provides startup permissions for specified users and defines how the bot auto-assigns privileges. You should put your name in here as an owner and put names of bot accounts you plan to use in the bots section (e.g. MyHub, MyBot1, MyBot2).
    * (Optional) `racism.cfg`  
        * This file is a list of fragments that will be flagged by racism detection.
    * `sql.cfg`  
        * This file contains the settings for working with a database. _If you do not (want to) use a database_, set `ConnectionCount` to 0. For more information, see below.

 4. Configure `sql.cfg` (or just set `ConnectionCount` to 0):
    * `[[General]]`
        * `RetryCount`  
          Amount of retries a connection is tried before it will be given up
        * `ConnectionCount`  
          Number of connections defined in this file and/or number of connections that are used. Set to 0 if you do not want to use a database.
    * `[Connection X]`  (Replace X for a number starting with 1, also with settings below)
        * `NameX`  
        Name of the connection. This same name is also used by the bots. The connection names used by the bots that come with TWCore are `local`, `website` and `server`.
        * `DriverX`  
        Name of the sql driver. This is usually set to `com.mysql.jdbc.Driver` which you don't need to change unless you want to work with a different type database then MySQL.
        * `LoginX`  
        The username used for connecting to the database server.
        * `PasswordX`  
        The password used for connecting to the database server.
        * `DataBaseX`  
        The database name that should be used. Make sure this database already exist on the server.
        * `ServerX`   
        The IP or the address of the database server. If the database is on the same machine, set it to `localhost`. If the database is not on this machine, make sure you have the database server configured that it allows external connections.
        * `PortX`   
        The port used to communicating with the database server. If you are using a MySQL server then you can leave it set to `3306`.
        * `MinPoolSizeX`   
        The minimum number of connections required to be in the pool. You can leave the default set to `1`.
        * `MaxPoolSizeX`  
        The maximum number of connections of the connection pool. You can leave the default set to `10`.
        * `WaitIfBusyX`  
        Wait if the connection is busy. Can be set to `0` or `1`.[[BR]]  
        You can create several connections in this file, you only need to change the X in each setting to the respective connection number.

## Ready to run
If you have followed the steps up to this point then you should be ready to run!

Make sure your development zone is running and any MySQL databases are accessible before trying to run.

You can recompile your source with `ant` which will make a `bin/twcore.jar` file. You could run this jar manually using `java -jar bin/twcore.jar bin/setup.cfg` or simply use `ant run` which will do this for you.

Your HubBot should then log into your development zone!

## Spawning bots
To spawn a bot you must edit a configuration file for the bot you want to spawn.

In TWCore we have two names for a specific bot: the bot _type_ and the bot _name_.   
The bot type is the name of the directory. The main `.java` and `.cfg` file within this directory both have the same name as the bot type. You use this bot type to specify which bot to spawn. The bot name is the name you configure inside the `.cfg` file.

To spawn a bot follow the following steps:

 1. The first time you spawn a bot you have to rename the `.example.cfg` file in its specific directory to `.cfg`  
 (e.g. `src/twcore/bots/multibot/multibot.example.cfg`).  
 1. Edit the `.cfg` file and specify the bot name and password. You can set the initial arena to 0 for a public arena if desired.
 1. Start up TWCore if you haven't already.
 1. PM `!spawn <bottype>` to your HubBot (e.g. `!spawn multibot`).
 1. If the bot doesn't spawn into your zone, check the TWCore console for error messages.

Want to see your changes take effect in Trench Wars? Take a look at the [Contribute guide](/Contribute) for contributing.

Problems while following this guide? Ask your question at the discussion forum!