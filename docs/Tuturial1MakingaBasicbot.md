---
disable_toc: true
---
<!-- Name: Tuturial1MakingaBasicbot -->
<!-- Version: 3 -->
<!-- Last-Modified: 2007/07/31 15:57:31 -->
<!-- Author: Maverick -->

(This tutorial has been copied with the approval of D1st0rt from [D1st0rt's website](http://d1st0rt.sscentral.com).)

# Tutorial 1: Making a basic bot
''Written by D1st0rt @ Friday, 19 November 2004  
Last update: Monday, 31 January 2005''

In this step, you'll see how to make simple bot that just logs in. Create a folder in your bots folder called mybot. Then create this file mybot.java:

```java
//Make the package name the same as the bot's name so you can 
//spawn it 
package twcore.bots.mybot;
//Import all of the TWCore classes so you can use them 
import twcore.core.*;

public class mybot extends SubspaceBot
{
    //Creates a new mybot 
    public mybot(BotAction botAction)
    {
        //This instantiates your BotAction 
        super(botAction);
    }

    //What to do when the bot logs on
    public void handleEvent(LoggedOn event)
    {
        //Get the data from mybot.cfg
        BotSettings config = m_botAction.getBotSettings();
        //Get the initial arena from config and enter it 
        String initial = config.getString("InitialArena");
        m_botAction.joinArena(initial);
        //NOTE: m_botAction is inherited from SubspaceBot 
    }
}
```

This is the the most basic bot you can make. You should familiarize yourself with m_botAction because you're going to be using it a lot. It is what enables you to interact with the game and do many useful things. Notice how in the LoggedOn event handler, you must join an arena. If you don't do this, your bot won't show up.

[Next Tutorial](/Tutorial2HandlingEvents)