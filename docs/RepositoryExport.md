<!-- Name: RepositoryExport -->
<!-- Version: 6 -->
<!-- Last-Modified: 2007/06/06 09:40:05 -->
<!-- Author: Maverick -->
### How to get a copy of source code from the Subversion repository
*(TODO)*

Note: After exporting your project from the subversion repository with Eclipse make sure you set your project settings to java version 5 compability:   

 1. Right-click the TWCore project in the Java view
 1. Select _Properties_
 1. Click on _Java Compiler_ in the tree left
 1. Enable _Enable project specific settings_
 1. Select _5.0_ as _Compiler compliance level_ and make sure _Use default compliance settings_ is enabled.
 1. Click yes if Eclipse asks you to rebuild the project.

If you still get errors about for example missing class `java.lang.Enum`, make sure you have the correct JRE set:

 1. Right-click the TWCore project in the Java view
 1. Select _Properties_
 1. Click on _Java Build Path_ in the tree left
 1. Click on the tab _Libraries_ and select the _JRE System Library_.
 1. Click on the button _Edit..._ on the right
 1. Choose a JRE. Usually the Workspace default JRE works as long as it is 1.5.0 or higher.
 1. Close the window by clicking _Finish_
 1. Click _OK_
