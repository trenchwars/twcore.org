<!-- Name: SubspaceProtocol -->
<!-- Version: 50 -->
<!-- Last-Modified: 2015/03/12 11:40:04 -->
<!-- Author: JoWie -->
# Subspace Protocol Packets

Original source: http://d1st0rt.sscentral.com/packets.html   
Copied to twcore.org for further modification and addition.

General notes:

 * Maximum packet size is 520
 * All strings are padded with 0x00 characters

  
## C2S : Client to Server
### Subspace packets
#### 0x00 - Start of a special header
 _See: [#BI:Bi-directionalCorePackets Core Packets] or manually scroll down (Near the end) to the core packets section._


#### 0x01 - Arena login
 | Offset | Length | Description           |
 |--------|--------|-----------------------|
 | 0      | 1      | Type Byte 0x01        |
 | 1      | 1      | Ship type ^1^         |
 | 2      | 2      | Allow audio ^2^       |
 | 4      | 2      | X Resolution          |
 | 6      | 2      | Y Resolution          |
 | 8      | 2      | Main arena number ^3^ |
 | 10     | 16     | Arena name (optional) |
 
 1 - Ship type is 0 for warbird up to 8 for spectator  
 2 - When Allow audio = 0, remote messages aren't heard in private arenas (1.34.11f)  
 3 - Set to Pub Number for Specific Pub, Set to 0xFFFF for Random Pub, Set to 0xFFFD for private arena. Arena name is only used with private arenas.


#### 0x02 - Leave arena
 | Offset | Length | Description     |
 |--------|--------|-----------------------|
 | 0      | 1      | Type Byte 0x02  |

#### 0x03 - Position packet
 | Offset | Length | Description                |
 |--------|--------|-----------------------|
 | 0      | 1      | Type Byte 0x03             |
 | 1      | 1      | Direction (0 ... 360)      |
 | 2      | 4      | Timestamp                  |
 | 6      | 2      | X Velocity                 |
 | 8      | 2      | Y Pixels (0 ... 16384)     |
 | 10     | 1      | Checksum                   |
 | 11     | 1      | Togglables ^1^             |
 | 12     | 2      | X Pixels (0 ... 16384)     |
 | 14     | 2      | Y Velocity                 |
 | 16     | 2      | Bounty                     |
 | 18     | 2      | Energy                     |
 | 20     | 2      | Weapon Info ^2^            |
 | 22     | 2      | Energy ^4^ (Optional)      |
 | 24     | 2      | S2C Latency ^4^ (Optional) |
 | 26     | 2      | Timer ^4^ (Optional)       |
 | 28     | 4      | Item info ^3^ ^4^ (Optional) |
 
 ^1^ - Togglables:
  Each value is one bit in a byte  
  Bit 1   - Stealth  
  Bit 2   - Cloak  
  Bit 4   - XRadar  
  Bit 8   - Antiwarp  
  Bit 16  - Flash (Play the cloak/warp flash)  
  Bit 32  - Safety (In safe)  
  Bit 64  - UFO (Using UFO)  
  Bit 128 - ?

 ^2^ - Weapon info:
  Unsigned Integer Values or Booleans
  | Weapon type                 | 5 bits ^a1^ |
  | Weapon level                | 2 bits      |
  | Bouncing shrapnel (Boolean) | 1 bit       |
  | Shrapnel level              | 2 bits      |
  | Shrapnel amount             | 5 bits      |
  | Alternate (Boolean)         | 1 bit ^a2^  |
  
  ^a1^ - Weapon types:
   0x00 - None  
   0x01 - Bullet  
   0x02 - Bouncing bullet  
   0x03 - Bomb  
   0x04 - Proximity bomb  
   0x05 - Repel  
   0x06 - Decoy  
   0x07 - Burst  
   0x08 - Thor  
  ^a2^ - Alternate:
   Bombs -> Mines  
   Bullets -> Multifire
  
 ^3^ - Item info:
  Unsigned Integer Values or Booleans  
  | Shields (Boolean) | 1 bit  |
  | Super (Boolean)   | 1 bit  |
  | Burst Count       | 4 bits |
  | Repel Count       | 4 bits |
  | Thor Count        | 4 bits |
  | Brick Count       | 4 bits |
  | Decoy Count       | 4 bits |
  | Rocket Count      | 4 bits |
  | Portal Count      | 4 bits |
  | ? Unknown         | 2 bits |
 
 ^4^ - These are supposed to be optional, but I'm not certain what dictates if you should send them or not.
  One of the possible requirements for this seems to be whether or not you are spectated by someone with 'sysop-like' powers, to display all the additional data for them.


#### 0x04 - Unknown
#### 0x05 - Death message
 | Offset | Length | Description                  |
 |--------|--------|-----------------------|
 | 0      | 1      | Type Byte 0x05               |
 | 1      | 2      | Killer's Player ID           |
 | 3      | 2      | Your bounty at time of death |

#### 0x06 - Chat message
 | Offset | Length      | Description                   |
 |--------|--------|-----------------------|
 | 0      | 1           | Type Byte 0x06                |
 | 1      | 1           | Chat Type ^1^                 |
 | 2      | 1           | Sound Byte                    |
 | 3      | 2           | Target Player's Player ID ^2^ |
 | 5      | * [250 max] | Text... ^3^                   |
 | *      | 1           | Null Terminator 0x00 Required |
 
 ^1^ - Chat types:
  0x00 - Message in green text [*arena, *zone, ...]  
  0x01 - Public macro  
  0x02 - Public message  
  0x03 - Team message ![// or ']  
  0x04 - Player to all members of another team [["Whatever]]  
  0x05 - Private message ![[-Whatever]]  
  0x06 - Red warning message [MODERATOR WARNING: Whatever -Whoever]  
  0x07 - Remote private message	^a^ [(Whoever)> Whatever]  
  0x08 - Red server errors, without a name tag (S2C only)  
  0x09 - Channel message ^a^ [[X;Whatever]]  
  ^a^ - These are formatted much as you'd expect. To send a Remote Message, you include the name of the person you are sending the message to in the Text of the message like so: ":Name:Message" and with Channel Messages you include the Number of the Channel you are sending to, like so: "#;Message".

 ^2^ - Target player's player ID is 0x00 for the Following Chat Types: 0x00, 0x01, 0x02, 0x03, 0x06, 0x07, 0x08, 0x09. It is PlayerID for 0x05 and 0x04.

 ^3^ - The used character encoding (Character Set) for this seems to be [ISO-8859-1](http://en.wikipedia.org/wiki/ISO-8859-1), albeit continuum has some extended support, for example € does work, and some other characters, like ½ and ©, are seemingly not supported.

Although the maximum characters for this packet seems to be 243 characters.

(In Continuum you can send 249 characters no matter the caps lock).

In ASSS if you send over 243 the server will break synchronization with your packet stream then start spamming you with

`M <net> [[PlayerName]] [[pid=0]] unknown network subtype 0` indicating that the last null terminator is used to begin a new packet.

You can send chat messages up to 250 characters long (any more then that doesn't work, it even bypasses the continuum 249 character limit by one).

I've figured it out, nothing wrong with `ASSS` or `subgame` but the problem lies with the botcores themselves.
The packets in botcores I used like `OpenCore` and `twcore` have a overlooked problem with the implementation of the packet clustering. There lies a overflow. The Cluster packet can only hold multiple packets as long as each clustered packet doesn't exceed 255 bytes, but the packet clustering system doesn't take into account packets already with higher then 255 bytes to begin with so problems start happening there where the size byte starts to overflow into some random value and then the packet stream synchronization breaks this can also cause the server to recycle.

#### 0x07 - Take Green / Prize
 | Offset | Length | Description     |
 |--------|--------|-----------------------|
 | 0      | 1      | Type Byte 0x07  |
 | 1      | 4      | Timestamp       |
 | 5      | 2      | X               |
 | 7      | 2      | Y               |
 | 9      | 2      | Prize           |

#### 0x08 - Spectate player
 | Offset | Length | Description                  |
 |--------|--------|-----------------------|
 | 0      | 1      | Type Byte 0x08               |
 | 1      | 2      | Spectated Player's Player ID |

#### 0x09 - Password packet
 | Offset | Length | Description                     |
 |--------|--------|-----------------------|
 | 0      | 1      | Type Byte 0x09                  |
 | 1      | 1      | New user (Boolean) ^1^          |
 | 2      | 32     | Name ^2^                        |
 | 34     | 32     | Password ^2^                    |
 | 66     | 4      | MachineID ^3^                   |
 | 70     | 1      | ConnectType ^4^                 |
 | 71     | 2      | Timezone bias ^5^               |
 | 73     | 2      | ? Unknown                       |
 | 75     | 2      | Client version ^6^              |
 | 77     | 4      | Mem Checksums [[0x01BC]] or ![[444]]|
 | 81     | 4      | Mem Checksums [[0x022B]] or ![[555]]|
 | 85     | 4      | PermissionID                    |
 | 89     | 12     | ? Unknown                       |

 ^1^ - New user is 1 if you want to create a new user, otherwise 0

 ^2^ - Name and password are trimmed down to 20/24 characters on some servers/billers

 ^3^ - MachineID is the drive serial of C:

 ^4^ - Connect types:
  0x00 - Unknown  
  0x01 - SlowModem  
  0x02 - FastModem  
  0x03 - UnknownModem  
  0x04 - UnknownNotRAS

 ^5^ - Time Zone Bias
  240 = Eastern Standard Time (Safe Value)

 ^6^ - Client versions:
  0x24 - Continuum .36  
  0x86 - Subspace 1.34 And 1.35

#### 0x0A - Unknown
#### 0x0B - SSUpdate.EXE Request
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0B |

#### 0x0C - Map.lvl Request
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0C |

#### 0x0D - News.txt Request
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0D |

#### 0x0E - Voice Message
 | Offset | Length | Description                      | 
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0E                   | 
 | 1      | 1      | Voice Upload Index (0 to 3 Only) ^1^ | 
 | 2      | 2      | Player Id (Less then 1024)       | 
 | 4      | *      | Wav File Bytes... ^2^            | 

 ^1^ - Total of 4 Wav File Uploads per Player.  
 ^2^ - Size of the Wav File is calculated by *(PacketSize - 4)*


#### 0x0F - Frequency Change
 | Offset | Length | Description    | 
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0F | 
 | 1      | 2      | New Frequency  | 

#### 0x10 - Attach request
 | Offset | Length | Description            | 
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x10         | 
 | 1      | 2      | Player ID of Turretee  | 

#### 0x11 - Unknown
#### 0x12 - Unknown
#### 0x13 - Flag request
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x13 |
 | 1      | 2      | Flag ID        |

#### 0x14 - Drop all attached pilots
This packet has no payload once received by server, sends 0x15 - Destroy Turret Link to everybody who was attached.

#### 0x15 - Drop Flags
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x15 |

#### 0x16 - File transfer
 | Offset | Length | Description             |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x16          |
 | 1      | 16     | File Name               |
 | 17     | *      | ZLib Compressed File... |

#### 0x17 - Registration Form Response
 | Offset | Length | Description                        |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x17                     |
 | 1      | 32     | Real Name ^1^                      |
 | 33     | 64     | E-Mail ^1^                         |
 | 97     | 32     | City ^1^                           |
 | 129    | 24     | State ^1^                          |
 | 153    | 1      | Sex ^2^                            |
 | 154    | 1      | Age                                |
 | 155    | 1      | Connecting From: Home (Boolean)    |
 | 156    | 1      | Connecting From: Work (Boolean)    |
 | 157    | 1      | Connecting From: School (Boolean)  |
 | 158    | 4      | Processor Type                     |
 | 162    | 4      | ? Unknown                          |
 | 166    | 40     | Windows Registration: Real Name    |
 | 206    | 40     | Windows Registration: Organization |
 |        |        | [[BR]] The following are registry entries and can be found in the following key: [[BR]] `SystemCurrentControlSetServicesClass` |
 | 246    | 40     | Display000:DriverDesc            |
 | 286    | 40     | Monitor000:DriverDesc            |
 | 326    | 40     | Modem000:DriverDesc              |
 | 366    | 40     | Modem001:DriverDesc              |
 | 406    | 40     | Mouse000:DriverDesc              |
 | 446    | 40     | Net000:DriverDesc                |
 | 486    | 40     | Net001:DriverDesc                |
 | 526    | 40     | Printer000:DriverDesc            |
 | 566    | 40     | MEDIA000:DriverDesc              |
 | 606    | 40     | MEDIA001:DriverDesc              |
 | 646    | 40     | MEDIA002:DriverDesc              |
 | 686    | 40     | MEDIA003:DriverDesc              |
 | 726    | 40     | MEDIA004:DriverDesc              |

 ^1^ - Real name, E-mail, City, State are null-terminated strings

 ^2^ - Sex is either 'F' or 'M'

#### 0x18 - Set ship type
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x18 |
 | 1      | 1      | Ship Type ^1^  |

 ^1^ - Ship type is 0 for Warbird through 8 for Spectator

#### 0x19 - Set personal banner
 | Offset | Length | Description     |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x19  |
 | 1      | 96     | Banner Data ^1^ |

 ^1^ - Banner data is the value for each pixel in the banner. (Banner size is 12x8 pixels = 96 pixels.) Uncompressed BMP Files follow this format.

#### 0x1A - Security checksum
 The checksums are generated after a server-sent seed

 | Offset | Length | Description                   |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1A                |
 | 1      | 4      | Weapon Count                  |
 | 5      | 4      | Settings Checksum ^1^         |
 | 9      | 4      | Subspace.EXE Checksum         |
 | 13     | 4      | Map.LVL Checksum              |
 | 17     | 4      | S2CSlowTotal                  |
 | 21     | 4      | S2CFastTotal                  |
 | 25     | 2      | S2CSlowCurrent                |
 | 27     | 2      | S2CFastCurrent                |
 | 29     | 2      | S2CRelOut (?Unsure?)          |
 | 31     | 2      | Ping                          |
 | 33     | 2      | Ping Average                  |
 | 35     | 2      | Ping Low                      |
 | 37     | 2      | Ping High                     |
 | 39     | 1      | Slow Frame Detected (Boolean) |

 ^1^ - Settings checksum is the checksum of the contents of the settings packet

#### 0x1B - Security violation
 | Offset | Length | Description     |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1B  |
 | 1      | 1      | Violation ID ^1^|

 ^1^ - Violation IDs:
  These may only be sent in response to a security checksum request
   0x00 - Nothing wrong  
   0x01 - Slow framerate  
   0x02 - Current energy is higher than top energy  
   0x03 - Top energy higher than max energy  
   0x04 - Max energy without getting prizes  
   0x05 - Recharge rate higher than max recharge rate  
   0x06 - Max recharge rate without getting prizes  
   0x07 - Too many burst used (More than you have)  
   0x08 - Too many repel used  
   0x09 - Too many decoy used (More than you have)  
   0x0A - Too many thor used (More than you have)  
   0x0B - Too many wall blocks used (More than you have)  
   0x0C - Stealth on but never greened it  
   0x0D - Cloak on but never greened it  
   0x0E - XRadar on but never greened it  
   0x0F - AntiWarp on but never greened it  
   0x10 - Proximity bombs but never greened it  
   0x11 - Bouncing bullets but never greened it  
   0x12 - Max guns without greening  
   0x13 - Max bombs without greening  
   0x14 - Shields or Super on longer than possible

  These can be sent any time
   0x15 - Saved ship weapon limits too high (burst/repel/etc)  
   0x16 - Saved ship weapon level too high (guns/bombs)  
   0x17 - Login checksum mismatch (program exited)  
   0x18 - Unknown  
   0x19 - Saved ship checksum mismatch

  These may only be sent in response to a security checksum request
   0x1A - Softice Debugger Running  
   0x1B - Data checksum mismatch  
   0x1C - Parameter mismatch  
   0x.... - Unknown integrity violation  
   0x3C - Unknown integrity violation (High latency in Continuum)

#### 0x1C - Drop Brick
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1C |
 | 1      | 2      | X Tiles        |
 | 3      | 2      | Y Tiles        |

#### 0x1D - Change Certain Arena Settings
 | Offset | Length | Description                               |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1D                            |
 | 1      | *      | "Weasel:SoccerBallSpeed:9000\0"           |
 | *      | *      | "Warbird:MaximumEnergy:2800\0" (Optional) |
 | *      | 2      | "\0\0" at the end of message              |

You could send multiple settings changes at once using this packet by repeating Offset 1 with null terminated byte at the end.


#### 0x1E - End Personal KoTH Timer
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1E |

#### 0x1F - Fire a Ball
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1F |
 | 1      | 1      | Ball ID        |
 | 2      | 2      | X Pixels       |
 | 4      | 2      | Y Pixels       |
 | 6      | 2      | X Velocity     |
 | 8      | 2      | Y Velocity     |
 | 10     | 2      | Your Player ID |
 | 12     | 4      | Timestamp      |

#### 0x20 - Ball request
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x20 |
 | 1      | 1      | Ball ID        |
 | 2      | 4      | Timestamp      |

#### 0x21 - Soccer goal scored
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x21 |
 | 1      | 1      | Ball ID        |
 | 2      | 4      | Timestamp      |

#### 0x22 - Security Violation Unknown
 | Offset | Length | Description             |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x22          |
 | 1      | 4      | Unknown                 |
 | 5      | 4      | Arena Settings Checksum |
 | 9      | 4      | Code Checksum Unknown 1 |
 | 13     | 4      | Code Checksum Unknown 2 |
 | 17     | 1      | Security Violation Id   |

  
### Continuum packets
#### 0x23 - Unknown
#### 0x24 - Password packet
 Don't use this, its just here for completeness. Use the [#a0x09-Passwordpacket 0x09 Password Packet] instead.

 | Offset | Length | Description         |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte           |
 | 1      | 1      | Boolean: New user   |
 | 2      | 32     | Name                |
 | 34     | 32     | Password            |
 | 66     | 4      | Machine ID          |
 | 70     | 1      | ConnectType (*info) |
 | 71     | 2      | Time Zone Bias      |
 | 73     | 2      | ?                   |
 | 75     | 2      | Client Version      |
 | 77     | 87     | ?                   |

#### 0x35 - LVZ Object Toggle
Sends out one or more LVZ toggle states.   
Please note: TWCore sends this through packet 0x0A.

 | Offset | Length | Description                    |
 |--------|--------|----------------|
 | 0      | 1      | Encapsulating Type Byte (0x0A) |
 | 1      | 2      | Player ID ^1^                  |
 | 3      | 1      | Real Type Byte (0x35)          |
 | 4      | 2*     | Object Toggle Data ^2^         |

 ^1^ - Player ID:
  To send to a specific player, use that player's ID.  
  To send to all players, use -1.  
 ^2^ - Object Toggle Data:
  Contains one or multiple object toggle assignments, 2 bytes wide, according to the following structure:
  | Bit Field | Bit Length | Description                            |
  |--------|--------|----------------|
  | 0         | 15         | LVZ Object ID                          |
  | 15        | 1          | New state: 0 for hidden, 1 for visible |
  

#### 0x36 - LVZ Object Modification
Sends out one or more LVZ object modifications.   
Please note: TWCore sends this through packet 0x0A.

 | Offset | Length | Description                    |
 |--------|--------|----------------|
 | 0      | 1      | Encapsulating Type Byte (0x0A) |
 | 1      | 2      | Player ID ^[#point0x36_1 1]^                  |
 | 3      | 1      | Real Type Byte (0x36)          |
 | 4      | 11*    | Object Modification Data ^[#point0x36_2 2]^   |

 ^[=#point0x36_1 1]^ - Player ID:
  To send to a specific player, use that player's ID.  
  To send to all players, use -1.  
 ^[=#point0x36_2 2]^ - Object Modification Data:
  Contains one or multiple object modification assignments, 11 bytes wide, according to the following structure:
  | Field | Length | Description                 |
  |--------|--------|----------------|
  | 0     | 1      | Update Flags                |
  | 1     | 2      | Object ID & Object Type     |
  | 3     | 2      | Location [& Type] (x coord) |
  | 5     | 2      | Location [& Type] (y coord) |
  | 7     | 1      | LVZ Image ID                |
  | 8     | 1      | LVZ Layer                   |
  | 9     | 2      | Display Time & Mode         |
  
  
## S2C : Server to Client
### Subspace packets
#### 0x00 - Start of a special header
 _See: [#BI:Bi-directionalCorePackets Core Packets] or manually scroll down (Near the end) to the core packets section._

#### 0x01 - PlayerID Change
 | Offset | Length | Description       |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x01    |
 | 1      | 2      | New Player ID ^1^ |

 ^1^ - This is used to notify you of what your Player ID is.


#### 0x02 - You are now in the game
 | Offset | Length | Description        |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x02 ^1^ |

 ^1^ - At this point you can start sending position packets.


#### 0x03 - Player(s) Entering ^1^
 | Offset | Length | Description         |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x03      |
 | 1      | 1      | Ship Type           |
 | 2      | 1      | ? Unknown (0x00)    |
 | 3      | 20     | Name                |
 | 23     | 20     | Squad               |
 | 43     | 4      | Kill Points         |
 | 47     | 4      | Flag Points         |
 | 51     | 2      | User ID             |
 | 53     | 2      | Frequency           |
 | 55     | 2      | Wins                |
 | 57     | 2      | Losses              |
 | 59     | 2      | Attachee ID         |
 | 61     | 2      | Flags Held          |
 | 63     | 1      | Has KOTH Timer      |

 ^1^ - Upon entering an arena (or possibly some other instances) several of these packets (including the Type Byte) are stacked on top of each other and sent as one packet. Be sure to process this packet in 64 byte chunks until there are no chunks left.



#### 0x04 - Player Leaving
 | Offset | Length | Description     |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x04  |
 | 1      | 2      | Player ID       |


#### 0x05 - Large Position Packet (Weapons Packet)
 | Offset | Length | Description               |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x05            |
 | 1      | 1      | Direction (0-360)         |
 | 2      | 2      | Time Stamp                |
 | 4      | 2      | X Pixels (0-16384)        |
 | 6      | 2      | Y Velocity                |
 | 8      | 2      | Player ID                 |
 | 10     | 2      | X Velocity                |
 | 12     | 1      | Checksum                  |
 | 13     | 1      | Togglables ^1^            |
 | 14     | 1      | Ping                      |
 | 15     | 2      | Y Pixels (0-16384)        |
 | 17     | 2      | Bounty                    |
 | 19     | 1      | Weapon Parameters         |
 | 20     | 1      | Weapon Type               |
 | 21     | 2      | Energy ^2^ ^3^ (Optional) |
 | 23     | 2      | S2C Lag ^2^ (Optional)    |
 | 25     | 2      | Timer ^2^ (Optional)      |
 | 27     | 4      | Item Info ^2^ (Optional)  |

 ^1^ - Bit flags for the Togglables byte in bit order.
       | Stealth:   | 1;   |
       | Cloaked:   | 2;   |
       | XRadar:    | 4;   | (XRadar calcs are done client-side)    |
       | Antiwarp:  | 8;   | (Antiwarp calcs are done client-side)  |
       | WarpFlash: | 16;  | (Uncloaking, portaling, etc.)          |
       | UNKNOWN1:  | 32;  | ?                                      |
       | UFO:       | 64;  | (*ufo - Illegal usage caught in sg9+)  |
       | UNKNOWN2:  | 128; | ?                                      |

 ^2^ - This data is only sent if the ExtraPositionData flag is set to true in the zone settings.

 ^3^ - This data is sent if either the ExtraPositionData flag is set to true, or if you are using the Energy Password.

#### 0x06 - Player Death
 | Offset | Length | Description                 |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x06              |
 | 1      | 1      | ID of green left by death   |
 | 2      | 2      | Killer ID                   |
 | 4      | 2      | Killed ID                   |
 | 6      | 2      | Bounty ^1^                  |
 | 8      | 2      | Number of flags transferred |

 ^1^ - Add this to the killer's kill points.

 The death green is the ID of the green that is left by this players death. Killed ID is the ID of the player that got killed.
 The bounty, along with the other kill score modifiers in settings are added to the players kill points. The flags are how many flags were transferred as a result of the kill.

#### 0x07 - Chat
 | Offset | Length | Description       |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x07    |
 | 1      | 1      | Chat Type ^1^     |
 | 2      | 1      | Sound Byte        |
 | 3      | 2      | Originator ID ^2^ |
 | 5      | *      | Chat Message      |

 ^1^ - Chat types:
     0x00 - Message in green text [*arena, *zone, ...]  
     0x01 - Public macro  
     0x02 - Public message  
     0x03 - Team message ![// or ']  
     0x04 - Player to all members of another team ["Whatever]  
     0x05 - Private message ![/Whatever or :playername:Whatever]  
     0x06 - Red warning message [MODERATOR WARNING: Whatever -Whoever]  
     0x07 - Remote private message [(Whoever)> Whatever]  
     0x08 - Red server errors, without a name tag (S2C only)  
     0x09 - Channel message [;X;Whatever]  
 ^2^ - Target player's player ID is 0 for public messages (0x02)

#### 0x08 - Player got a Prize
 | Offset | Length | Description     |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x08  |
 | 1      | 4      | Timestamp       |
 | 5      | 2      | X Tiles         |
 | 7      | 2	     | Y Tiles         |
 | 9      | 2      | Prize           |
 | 11     | 2      | Player ID       |


#### 0x09 - Player Score Update
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x09 |
 | 1      | 2      | Player ID      |
 | 3      | 4      | Kill Points    |
 | 7      | 4      | Flag Points    |
 | 11     | 2      | Wins           |
 | 13     | 2      | Losses         |


#### 0x0A - Password Packet Response
 | Offset | Length | Description                         |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0A                      |
 | 1      | 1      | Login Response ^1^                  |
 | 2      | 4      | Server Version ^2^                  |
 | 6      | 4      | ? Unknown                           |
 | 10     | 4      | Subspace.exe Checksum ^3^           |
 | 14     | 4      | ? Unknown                           |
 | 18     | 1      | ? Unknown                           |
 | 19     | 1      | Registration Form Request (Boolean) |
 | 20     | 4      | Scrty file 4th offset DWORD         |
 | 24     | 4      | News.txt Checksum ^4^               |
 | 28     | 8      | ? Unknown (Random dust from memory) |

  ^1^ - The following is a list of what all the different Response codes mean:
        0x00 - Login OK  
        0x01 - Unregistered Player ^a1^  
        0x02 - Bad Password  
        0x03 - Arena is Full  
        0x04 - Locked Out of Zone  
        0x05 - Permission Only Arena  
        0x06 - Permission to Spectate Only  
        0x07 - Too many points to Play here  
        0x08 - Connection is too Slow  
        0x09 - Permission Only Arena  
        0x0A - Server is Full  
        0x0B - Invalid Name  
        0x0C - Offensive Name  
        0x0D - No Active Biller ^a2^  
        0x0E - Server Busy, try Later  
        0x0F - Not enough usage to play here  
        0x10 - Restricted Zone ^a3^  
        0x11 - Demo Version Detected  
        0x12 - Too many Demo users  
        0x13 - Demo Versions not Allowed  
        0xFF - Restricted Zone, Mod Access Required  
          
	^a1^ - Some Billers require you to register and will kick you out after a short time if you do not send the Registration form.  
        ^a2^ - You can still log in, but scores are not being permanently recorded.  
        ^a3^ - This restriction is usually based on insufficient hours of Usage.  
  ^2^  - Returns the Major and the Minor version as a single number (so 1.34.12a returns 134) does not return Sub Version..  
  ^3^  - Compare against local Subspace.exe to determine if an Update is needed. (obviously useless with a bot)  
  ^4^  - Compare against local News.txt to determine if there is a new News.txt to be downloaded.

#### 0x0B - Soccer Goal Made
 | Offset | Length | Description                  |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0B               |
 | 1      | 2      | Frequency that made the goal |
 | 3      | 4      | Team Points                  |



#### 0x0C - Player Voice
 | Offset | Length | Description     |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0C  |
 | 1      | 2      | Player ID       |
 | 3      | *      | Wav File...     |


#### 0x0D - Player Changed Frequency
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0D |
 | 1      | 2      | Player ID      |
 | 3      | 2      | Frequency      |

#### 0x0E - Create Turret Link
 | Offset | Length | Description                        |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0E                     |
 | 1      | 2      | Turreter Requester Player ID       |
 | 3      | 2      | Turreter Destination Player ID ^1^ |

 ^1^ - If this value is left blank, you will be detached from whoever you are attached to.

#### 0x0F - Arena Settings
 | Offset | Length | Description               |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x0F            |
 | 1      | 1428   | Arena Settings Struct ^1^ |

 ^1^ - This information has been lost (maybe it can be retrieved from [Mervbot](http://www.mervbot.com)?)

#### 0x10 - File Transfer
 | Offset | Length | Description      |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x10   |
 | 1      | 16     | File Name ^1^    |
 | 17     | *      | File Data... ^2^ |

 ^1^ - If no File Name is specified, then it's the News.txt  
 ^2^ - If the file is the News.txt it must be Uncompressed first. According to Snrrrub, all other files are sent Uncompressed.

#### 0x11 - Unknown, Possible NO-OP
#### 0x12 - Flag Position
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x12 |
 | 1      | 2      | Flag Id        |
 | 3      | 2      | X Tiles        |
 | 5      | 2      | Y Tiles        |
 | 7      | 2      | Owner ^1^      |

^1^ - The Team number that owns the flag.  
#### 0x13 - Flag Claim
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x13 |
 | 1      | 2      | Flag Id        |
 | 3      | 2      | Player Id      |

#### 0x14 - Flag Victory
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x14 |
 | 1      | 2      | Team           |
 | 3      | 4      | Jackpot Points |

#### 0x15 - Destroy Turret Link
 | Offset | Length | Description                 |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x15              |
 | 1      | 2      | Turret Driver Player ID ^1^ |

^1^ - This is sent from a client that has turret(s) attached to it and wants to get rid of them. Upon recieving this packet, you should check to see if you are attached to Player ID and if so, detatch.  
#### 0x16 - Drop Flag
 | Offset | Length | Description       |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x16    |
 | 1      | 2      | Carrier Player ID |

#### 0x17 - Unknown, Possible NO-OP
#### 0x18 - Synchronization Request
 | Offset | Length | Description          |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x18       |
 | 1      | 4      | Prize Seed Value ^1^ |
 | 5      | 4      | Door Seed Value ^1^  |
 | 9      | 4      | Timestamp            |
 | 13     | 4      | Checksum Key ^2^     |

^1^ - Seed values are used in Random Number Generators to determine the location of Prizes and the timing for door switching.  
^2^ - When this packet is recieved, a Security Checksum packet (0x1A) should be returned using the Server Checksum Key.

#### 0x19 - Request File
 | Offset | Length | Description          |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x19       |
 | 1      | 256    | Local File Name      |
 | 257    | 17     | Remote File Name ^1^ |

^1^ - Sent when the server wants a file from the client (as in *putfile). The file requested should be in the same directory as the EXE.

#### 0x1A - Reset Score(s)
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1A |
 | 1      | 2      | Player Id ^1^  |

^1^ - If player ID = 0xFFFF then reset everyone's scores.

#### 0x1B - Personal Ship Reset
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1B |

#### 0x1C - Put Player in Spectator Mode
 | Offset | Length | Description                                                  |
 | 0      | 1      | Type Byte 0x1C                                               |
 | 1>     | 2      | Player Id ^2^ (One of the following: ^1^)                    |
 | 1>     | 1      | Request Extra Pos Info (Boolean) (One of the following: ^1^) |

^1^ - Only one type of 1C is sent, check length to determine which one was sent. Length = 2 or 3.  
^2^ - I can only assume if the ID is yours, you should spec yourself.

#### 0x1D - Player Team and Ship Changed
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1D |
 | 1      | 1      | Ship Type      |
 | 2      | 2      | Player Id      |
 | 4      | 2      | Frequency      |

#### 0x1E - Personal Banner Changed
 | Offset | Length | Description              |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1E           |
 | 1      | 1      | Boolean ?Unknown? (0x00) |

#### 0x1F - Player Banner Changed
 | Offset | Length | Description          |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x1F       |
 | 1      | 2      | Player ID            |
 | 3      | 96     | Banner Data (Bitmap) |

#### 0x20 - Collected Prize
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x20 |
 | 1      | 4      | Timestamp      |
 | 5      | 2      | X Tiles        |
 | 7      | 2      | Y Tiles        |
 | 9      | 2      | Prize Type ^1^ |
 | 11     | 2      | Player Id      |

#### 0x20 - Buy Item From ?buy=prize Or Get Prized *prize #prize
Timestamp is divided to be used as ?buy=prize also used for *prize #prize

 | Offset | Length | Description     |
 | 0      | 1      | Type Byte 0x20  |
 | 1      | 2      | Amount of prize |
 | 3      | 2      | Prize Id ^1^    |

^1^ - Listed here from 0 to 28: Unknown, Recharge, Energy, Rotation, Stealth, Cloak, XRadar, Warp, Guns, Bombs, Bullets, Thruster, TopSpeed, FullCharge, EngineShutdown, Multifire, Proximity, Super, Shields, Antiwarp, Repel, Burst, Decoy, Thor, MultiPrize, Brick, Rocket, Portal.

#### 0x21 - Brick Dropped
 | Offset | Length | Description                    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x21                 |
 | 1      | 2      | X1 Tiles                       |
 | 3      | 2      | Y1 Tiles                       |
 | 5      | 2      | X2 Tiles                       |
 | 7      | 2      | Y2 Tiles                       |
 | 9      | 2      | Team                           |
 | 11     | 2      | Brick ID (sent more than once) |
 | 13     | 4      | Timestamp                      |

#### 0x22 - Turf Flag Update
 | Offset | Length | Description        |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x22     |
 | 1>     | 2      | Team for Flag X ^1^ |

^1^ - The following are repeated until the end of the message.

#### 0x23 - Flag Reward Granted
 | Offset | Length | Description                   |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x23                |
 | 1      | 2      | Team                          |
 | 3      | 2      | Points Awarded                |
 | 5>     | 2      | Team ^1^ (Optional)           |
 | 7>     | 2      | Points Awarded ^1^ (Optional) |

^1^ - The Team and Points Awarded field are repeated until all Point notifications are made. This packet can be of any length.

#### 0x24 - Speed Game Over
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x24 |
 | 1      | 1      | Best           |
 | 2      | 2      | Your Rank      |
 | 4      | 4      | Your Score     |
 | 8      | 4      | Player 1 Score |
 | 12     | 4      | Player 2 Score |
 | 16     | 4      | Player 3 Score |
 | 20     | 4      | Player 4 Score |
 | 24     | 4      | Player 5 Score |
 | 28     | 2      | Player 1 Id    |
 | 30     | 2      | Player 2 Id    |
 | 32     | 2      | Player 3 Id    |
 | 34     | 2      | Player 4 Id    |
 | 36     | 2      | Player 5 Id    |

#### 0x25 - Toggle UFO Ship
 | Offset | Length | Description        |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x25     |
 | 1      | 1      | Boolean Available? |

#### 0x26 - Unknown, Possible NO-OP
#### 0x27 - Keep-Alive
 | Offset | Length | Description        |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x27     |

#### 0x28 - Small Position Packet
 | Offset | Length | Description               |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x28            |
 | 1      | 1      | Direction (0 ... 360)     |
 | 2      | 2      | Timestamp                 |
 | 4      | 2      | X Pixels (0 ... 16384)    |
 | 6      | 1      | Ping                      |
 | 7      | 1      | Bounty                    |
 | 8      | 1      | Player Id ^1^             |
 | 9      | 1      | Togglables ^2^            |
 | 10     | 2      | Y Velocity ^3^            |
 | 12     | 2      | Y Pixels (0 ... 16384)    |
 | 14     | 2      | X Velocity ^3^            |
 | 16     | 2      | Energy ^4^ ^5^ (Optional) |
 | 18     | 2      | S2C Lag ^4^ (Optional)    |
 | 20     | 2      | Timers ^4^ (Optional)     |
 | 22     | 4      | Items ^4^ ^6^ (Optional)  |
 
 ^1^ - Since the Player ID here is only one byte, this packet is only capable of addressing the first 256 clients in a zone.  
 ^2^ - Togglables:
  Each value is one bit in a byte  
  Bit 1   - Stealth  
  Bit 2   - Cloak  
  Bit 4   - XRadar  
  Bit 8   - Antiwarp  
  Bit 16  - Flash (Play the cloak/warp flash)  
  Bit 32  - Safety (In safe)  
  Bit 64  - UFO (Using UFO)  
  Bit 128 - ?

 ^3^ - Velocitys are measured in Pixels per 10 Seconds.  
 ^4^ - These values are only recieved if the ExtraPositionData variable in the Settings is set to True.  
 ^5^ - This value is sent if either the ExtraPositionData value is set to True in the settings or if you are using the Energy Password.  
 ^6^ - Item info:
  Unsigned Integer Values or Booleans  
  | Shields (Boolean) | 1 bit  |
  | Super (Boolean)   | 1 bit  |
  | Burst Count       | 4 bits |
  | Repel Count       | 4 bits |
  | Thor Count        | 4 bits |
  | Brick Count       | 4 bits |
  | Decoy Count       | 4 bits |
  | Rocket Count      | 4 bits |
  | Portal Count      | 4 bits |
  | ? Unknown         | 2 bits |
 
#### 0x29 - Map Information
 | Offset | Length | Description                   |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x29                |
 | 1      | 16     | Map File Name                 |
 | 17     | 4      | Map Checksum ^1^              |
 | 21     | 4      | Map FileSize (compressed) ^2^ |

^1^ - If the Mapfile does not exist, send a Request Map packet. If it does exist, check its checksum against the one that came in this packet, if they do not match, Request the map. If they do match, load the map.   
^2^ This is a Continuum only extension parameter, doesn't exist in old Subspace protocol. Without sending this parameter on Continuum the Map download will always be at 0%.

#### 0x2A - Compressed Map File
 | Offset | Length | Description              |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x2A           |
 | 1      | 16     | Map File Name            |
 | 17     | *      | ZLib Compressed Map File |

#### 0x2B - Set Personal KotH Timer
 | Offset | Length | Description      |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x2B   |
 | 1      | 4      | New timer value  |

#### 0x2C - KotH Game Reset
 | Offset | Length | Description       |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x2C    |
 | 1      | 1      | Adding KoTH timer |
 | 2      | 4      | Timer value       |
 | 6      | 2      | Player Id         |

#### 0x2D - Add KotH time
 | Offset | Length | Description      |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x2D   |
 | 1      | 4      | Additional time  |

#### 0x2E - Power-Ball Position Update
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x2E |
 | 1      | 1      | Powerball Id   |
 | 2      | 2      | X Pixels       |
 | 4      | 2      | Y Pixels       |
 | 6      | 2      | X Velocity     |
 | 8      | 2      | Y Velocity     |
 | 10     | 2      | Owner Id       |
 | 12     | 4      | Timestamp      |

#### 0x2F - Arena Directory Listing
 | Offset | Length | Description                      |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x2F                   |
 | 1>     | *      | Arena Name (Null Terminated) ^1^ |
 | ?>     | 2      | Arena Population ^1^             |

^1^ - Arena Name and Arena Population are repeated until all arenas are enumerated.

#### 0x30 - Zone Banner Advertisements
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x30 |
 | 1      | 1      | Display Mode   |
 | 2      | 2      | Width          |
 | 4      | 2      | Height         |
 | 6      | 4      | Duration       |
 | 10     | *      | Banner Pixels ^1^     |

^1^ - No one seams to have any clue how to determine the end of this field. Nor does anyone know how this file is packed. (W*H+10 == Len)   
This field only contains pixel colors (1 byte per pixel), no palette is sent, it uses the default subspace palette of 256 colors.   
If you wish to receive the image you have to fill in 14 byte header for the BITMAP image, you can create this header by saving any image in MS-Paint with 256 colors.   
#### 0x31 - Post Login Sequence
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x31 |

### Continuum Packets
#### 0x32 - Change Personal Ship Coordinates
 | Offset | Length | Description    |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x32 |
 | 1      | 2      | X Tiles        |
 | 3      | 2      | Y Tiles        |

#### 0x33 - Custom Login Failure Message
#### 0x34 - Continuum Version Packet
 | Offset | Length | Description                       |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x34                    |
 | 1      | 2      | Continuum Version ^1^             |
 | 3      | 4      | Continuum.EXE Checksum (CRC32) ^2^|

^1^ The Current Continuum.exe version is `40`   
^2^ The Current Continuum.exe Checksum is `0xC9B61486` [Checksum is unsigned integer]


#### 0x35 - LVZ Object Toggling
#### 0x36 - LVZ Object Modification
#### 0x37 - Toggle Send Damage
#### 0x38 - Watch Damage
 | Offset | Length | Description     |
 |--------|--------|----------------|
 | 0      | 1      | Type Byte 0x38  |
 | 1      | 2      | Player ID       |
 | 3      | 4      | Timestamp       |
 | 7>     | 2      | Attacker ID ^1^ |
 | 9>     | 2      | Weapon Info ^1^ |
 | 11>    | 2      | Last energy ^1^ |
 | 13>    | 2      | Damage ^1^      |
 | 15>    | 1      | ? ^1^           |

^1^ - The following are repeated until the end of the message.

...

0x09  Player Score Update  
Watch out by this packet -> Flag Points and Kill Points are reversed

0x03  Player Entering  
Watch out -> Flag Points and Kill Points are reversed

## BI : Bi-directional Core Packets
### Subspace Packets
#### 0x01 - Encryption Request
 | Offset | Length | Description             |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x01    |
 | 2      | 4      | Client Encryption Key   |
 | 6      | 2      | Client Version/Type ^1^ |

^1^ - Protocol versions:  
	0x0001 - Subspace  
	0x0010 - Continuum Step 1 Array from Scrty1 File (Sends 1 Key for that Array)  
	0x0011 - Continuum Step 1 & 2 Array from Scrty1 File (Sends 2 Keys)  
The scrty1 file has the following format:

```
<version (4)>
{
    <key2 (4)>
    <step1 expansion (80)>
} * 1024
{
    <key2 (4)>
    <step1,2 expansion (80)>
} * 1024
```


#### 0x02 - Encryption Response
 | Offset | Length | Description             |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x02    |
 | 2      | 4      | Client Encryption Key   |

#### 0x02 - Encryption Response (Continuum)
 | Offset | Length | Description               |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x02      |
 | 2      | 4      | Always 0x00000000         |
 | 7      | 1      | Has billing? 0x00 or 0x01 |
 | 8      | 256    | Billing identity          |

If the billing identity is missing, the packet has a length of 7 bytes. The billing identity is related to the Zone Password warning you see for non ssc zones.

#### 0x03 - Reliable Message
 | Offset | Length | Description           |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x03  |
 | 2      | 4      | ACK ID                |
 | 6      | *      | Reliable Packet...    |

#### 0x04 - Reliable ACK
 | Offset | Length | Description          |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x04 |
 | 2      | 4      | ACK ID ^1^           |

^1^ The Reliable ACK packet is used both ways Client to Server and Server to Client to let both sides know which ACK ID they already received so it doesn't get re-sent again.

Since Reliable packets in case of packet-loss don't arrive they are synchronized by a ACK ID.[[br]]
In SubGame2 the ACK ID gets re-cycled every 256 elements, So if ACK ID value is up to 2311 it is really ACK ID value 7 because [2311 % 256] = 7.[[br]]
If packets with ACK ID 2,3,4,5 are on Queue for processing and the processing ACK ID is only on 1 it won't process until ACK ID 1 is recieved.

But it does process ACK ID's instantly if they are received in proper order to the processing ACK ID it doesn't wait to stack them up how I thought lol.

#### 0x05 - Sync Request To Sync Time
 | Offset | Length | Description            |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x05   |
 | 2      | 4      | Timestamp              |
 | 6      | 4      | Total Packets Sent     |
 | 10     | 4      | Total Packets Received |

#### 0x06 - Sync Response
 | Offset | Length | Description            |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x06   |
 | 2      | 4      | Received Timestamp     |
 | 6      | 4      | Local Timestamp        |

#### 0x07 - Order to Disconnect
 | Offset | Length | Description            |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x07   |

#### 0x08 - Small Chunk Body
 | Offset | Length | Description           |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x08  |
 | 2      | *      | Data ^1^              |

^1^ - Store the data in a buffer until you get a tail.

#### 0x09 - Small Chunk Tail
 | Offset | Length | Description           |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x09  |
 | 2      | *      | Data ^1^              |

^1^ - Now process the buffer

#### 0x0A - HUGE Chunk
 | Offset | Length | Description           |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x0A  |
 | 2      | 4      | Total Length          |
 | 6      | *      | Data ^1^              |

^1^ - Store data in a buffer until buffer length = Total Length.

#### 0x0B - Cancel Huge Chunk
#### 0x0C - Cancel Huge Chunk ACK
#### 0x0D - NO-OP
#### 0x0E - Packet Cluster
 | Offset | Length | Description          |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x0E |
 | 2>     | 1      | Packet Length ^1^    |
 | 3>     | *      | Packet... ^1^        |

^1^ - Packet Length and Packet are repeated for each packet in the packet cluster.  
This packet can be any length up to a maximum of 512 bytes.

### Continuum Packets
#### 0x10 - Continuum Encryption Response (Server)
 | Offset | Length | Description                               |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x10                      |
 | 2      | 4      | Server Key 1 (Could be any value) ^1^     |
 | 6      | 4      | Scrty1 Key (Either Step1 or Step1And2) ^2^|
 | 10     | 2      | 01 00 (Flag?) ^3^                         |

^1^ The Server Key 1 is the main key Continuum and the server uses to expand Step3 of the scrty1 key expansion. The server generates this key and sends it over to the client. It could be generated to any random value.   
In subgame server it is generated by seperating the 4 byte integer into 2 sections of 2 byte.   
First section indicates how much times you opened Continuum.   
Second section is just a pseudo-random number generator which is seeded with `GetTickCount()`.   
^2^ The Scrty1 Key is any of the random 4 byte key which is expanded from offset 0-1023 in the Scrty1 server-sided file.   
The Scrty1 file format contains 2 array expansion databases.   
First one is a old outdated version which is probably turned off contains only Step1 of expansion done for you still requires you to do Step2 and Step3 on the server, most likely due to the extra stress on the CPU this was disabled.   
This one is used when Protocol is set to `0x0010` in the packet `0x01 - Encryption Request`.  
The second which is currently used in `Continuum v0.40` is the bottom half of Scrty1 file which is called Step1AndStep2 also offseted from 0-1023, 4 byte key and of course 1024 of 80 byte expanded arrays for every key.
This newer version uses the Protocol `0x0011` in the packet `0x01 - Encryption Request`  
It is much easier to implement if you are building a server, since you only need to implement Step3 and not Step2 and Step3  
The scrty1 file has the following format:

```
<version (4)>
{
    <key2 (4)>
    <step1 expansion (80)>
} * 1024
{
    <key2 (4)>
    <step1,2 expansion (80)>
} * 1024
```

^3^ When the Protocol Version is set to `0x0011` then this `(Flag)` is set to 1 and packet length is `12`.  
When Protocol Version is set to `0x0010` then this Flag is completely omitted and packet is only length of `10`.   
#### 0x11 - Continuum Encryption Request (Client)
 | Offset | Length | Description              |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x11     |
 | 2      | 4      | Mimic's Server Key 1 ^1^ |
 | 6      | 2      | 01 00 (Flag?) ^2^        |

^1^ Mimicked Server Key 1 is from packet 0x10 the one above.   
The purpose of the mimicked server key is rather silly it's used as a extra Lookup serversided, other then IP Table lookup pretty much the Server Key never repeats itself for every user that connects it might start to roll over after thousands of connect attempts. So it's pretty useless. Considering they got Remote Port lookups which is far superior to tell connections apart I think the Flag may also be mimicked not sure though.

^2^ I Believe this flag information is similar to packet 0x10 in some way.


TODO:
Update this list to what we and others know now, and include what TWCore handles.
Also adding the higher packet numbers.

#### 0x12 - Continuum Encryption Key Expansion Request (Server)
 | Offset | Length | Description              |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x12     |
 | 2      | 4      | Seed                     |

Request the client to perform a key expansion using the given seed. 

The server has a big table of keys contained in scrty1. It periodically picks a new slot and asks multiple clients to perform key expansion on a seed it randomly generates (for that slot). If the clients agree on the key expansion the result (see 0x0013) is stored in scrty1 and used to encrypt packets.

#### 0x13 - Continuum Encryption Key Expansion Response (Client)
 | Offset | Length | Description              |
 |--------|--------|----------------|
 | 0      | 2      | Type Byte 0x00, 0x13     |
 | 2      | 2      | Version (0x0000)         |
 | 4      | 4      | Key                      |
 | 8      | 4*20   | Key table                |

The result of this is stored in scrty1 and used for encrypting packets.