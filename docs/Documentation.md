<!-- Name: Documentation -->
<!-- Version: 5 -->
<!-- Last-Modified: 2008/01/31 17:57:35 -->
<!-- Author: Maverick -->


# TWCore Documentation
Since documentation is important to every software project we try to document TWCore as much as possible.

However, as experienced TWCore developers it's hard to know what new people to TWCore need. Please add any subjects you want to see here or ask your questions at the public [/discussion Forum].

## Setting up and using TWCore
This guide tries to describe the steps necessary to get TWCore running on your computer and connected to your zone.  
Continue to [Setting up and using TWCore](SettingUp).

## Programming with TWCore
You can program in Java and want to make your own TWCore bots? This guide is everything you need to get started.   
Continue to [Programming with TWCore](Programming).

## Javadoc
Javadoc is a short term for Java Documentation. In the java code we use special comments to describe classes, methods and variables. All these comments are parsed together to a website which is the Java Documentation for TWCore. This Java Documentation is very useful when you are programming in TWCore for looking up certain classes/methods. See [Javadoc Tool](http://java.sun.com/j2se/javadoc/) for more information on Javadoc.  
Continue to [/javadoc TWCore's javadoc].

## Contributing to TWCore: Committing/submitting your changes
When you've created your awesome, fantastic, kick-ass bot we would be very gratefull if you share it so other people can use it aswell. This page explains how you should go around committing your changes to TWCore's source code repository.  
Continue to [Contributing to TWCore: Committing/submitting your changes](Contribute).