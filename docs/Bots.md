<!-- Name: Bots -->
<!-- Version: 38 -->
<!-- Last-Modified: 2009/09/06 16:18:54 -->
<!-- Author: Maverick -->


# TWCore's Bots
This list contains all the bots TWCore has with a link to their source in the repository.

Note: You need the appropiate authority to access the non-public bots.
Note2: Dates are written in the format of dd-mm-yyyy (european time notation). This date should be changed to a textual version of the month some time to avoid date notation confusion.

## Public bots
The following bots are included with each release and considered public:

| Name | Author | Date (first version) | Link to source file(s) |
|------|--------|----------------------|------------------------|
| [Alertbot](Alertbot) | ? | 18-3-2004 | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/alertbot/) [SQL](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/alertbot/alertbot.sql) |
| Bannerboy				| lnx			| 11-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/bannerboy/) |
| Basicbot (Template / Example bot)	| Stefan / Mythrandir	| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/basicbot/) |
| Bouncerbot				| ?			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/bouncerbot/) |
| BWJSBot				| fantus		| 29-03-2009	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/bwjsbot/) |
| Chatrelaybot				| Jacen Solo/Ikrit	| 19-7-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/chatrelaybot/) |
| Distensionbot				| dugwyler		| 10-1-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/distensionbot/) [SQL](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/distensionbot/distensionbot.sql) |
| Elim					| Milosh		| 15-09-2008	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/elim/) |
| Elimbot				| MMaverick		| 7-3-2007	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/elimbot/) |
| Eventbot				| MMaverick		| 25-6-2007	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/eventbot/) |
| ForumBot				| MMaverick		| 07-04-2009	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/forumbot/) |
| [HubBot (core bot)](HubBot)			| Mythrandir (original)	| 18-03-2004	| [HubBot.java](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/core/HubBot.java) |
| JavElim				| flibb			| 17-7-2007	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/javelim/) |
| Logbot				| Ice-Demon/Ayano	| 14-8-2007	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/logbot/) |
| [Messagebot](Messagebot)		| Jacen Solo/Ikrit	| 3-1-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/messagebot/) |
| [Multibot](Multibot)		| lnx			| 11-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/) |
| - [Multibot/acro](/Multibot/acro)	| D1st0rt		| 4-6-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/acro) |
| - Multibot/acro2                      | alinea                | 11-5-07       | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/acro2) |
| - [Multibot/baseassault](Multibot-baseassault)		| script	| 4-7-2005| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/baseassault) |
| - Multibot/bbj			| ?			| 3-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/bbj) |
| - Multibot/bfallout			| Sika			| 7-4-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/bfallout) |
| - Multibot/boggle                     | milosh                | 4-3-08        | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/boggle) |
| - Multibot/bountyhunter               | Jacen Solo/Ikrit      | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/bountyhunter) |
| - Multibot/bship			| D1st0rt		| 27-4-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/bship) |
| - Multibot/cnr			| qan			| 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/cnr) |
| - Multibot/commander                  | dugwyler              | 6-11-07       | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/commander) |
| - Multibot/conquer                    | harvey                | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/conquer) |
| - Multibot/dangerous			| qan			| 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/dangerous) |
| - Multibot/dodgeball			| MMaverick		| 14-07-2008	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/dodgeball) |
| - Multibot/enigma			| 2dragons		| 3-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/enigma) |
| - Multibot/fallout			| Sika			| 7-4-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/fallout) |
| - Multibot/freezetag			| Jacen Solo/Ikrit	| 11-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/freezetag) |
| - Multibot/gangsoftw			| qan			| 01-01-2009	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/gangsoftw) |
| - Multibot/gangwars			| ?			| 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/gangwars) |
| - Multibot/golden                     | Kyace                 | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/golden) |
| - Multibot/gravbomber			| ?			| 3-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/gravbomber) |
| - Multibot/heli			| ?			| ?		| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/heli) |
| - Multibot/hunt			| Thomas   		| 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/hunt) |
| - Multibot/killer			| qan			| 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/killer) |
| - Multibot/killrace                   | harvey                | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/killrace) |
| - Multibot/marco                      | Jacen Solo/Ikrit      | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/marco) |
| - Multibot/medorp                     | Lain                  | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/medorp) |
| - Multibot/payback                    | Jacen Solo/Ikrit      | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/payback) |
| - Multibot/pictionary                 | milosh                | 4-9-08        | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/pictionary) |
| - Multibot/platoon                    | Jacen Solo/Ikrit      | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/platoon) |
| - Multibot/prodem			| 2dragons		| 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/prodem) |
| - Multibot/rabbit			| Stultus               | 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/rabbit) |
| - Multibot/revenge                    | Stultus               | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/revenge) |
| - Multibot/rps			| ?			| 3-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/rps) |
| - Multibot/scramble                   | milosh                | 11-1-2007     | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/scramble) |
| - Multibot/soccerrace                 | Jacen Solo/Ikrit      | 5-24-04       | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/soccerrace) |
| - Multibot/spaceball			| ?			| 3-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/spaceball) |
| - Multibot/speed			| Sika			| 8-8-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/speed) |
| - Multibot/starcon			| ?			| 11-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/starcon) |
| - Multibot/stepelim                   | fantus                | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/stepelim) |
| - Multibot/tankwarfare		| fantus		| 10-04-2009	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/tankwarfare) |
| - Multibot/terror			| fantus		| 22-05-2009	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/terror) |
| - Multibot/tortuga			| alinea		| 10-12-2008	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/tortuga) |
| - Multibot/trivia			| 2dragons		| 3-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/trivia) [SQL](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/trivia/database.sql) |
| - Multibot/tugawar                    | Austin                | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/tugawar) |
| - Multibot/twscript                   | milosh                | 09-08-2008    | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/twscript) |
| - Multibot/untouchable		| qan			| 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/untouchable) |
| - Multibot/wiaw                       | milosh                | 4-5-08        | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/wiaw) |
| - Multibot/wipeout                    | Jacen Solo/Ikrit      | ?             | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/wipeout) |
| - Multibot/zombies			| harvey		| 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/zombies) |
| PracBot                               | milosh                | 1-15-2008     | [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/pracbot) |
| Racebot				| ?			| 11-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/racebot) |
| Radiobot				| flibb			| 21-5-2007	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/radiobot) |
| Robohelp				| Rodge_Rabbit		| 15-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/robohelp) [SQL](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/robohelp/robohelp.sql) |
| Roboreplacement			| Revived by qan	| 01-07-2008	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/roboreplacement) |
| Staffbot				| Mr. Spam		| 26-7-2002	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/staffbot) |
| [TWRCBot](TWRCBot)			| Jacen Solo/Ikrit	| 11-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/twrcbot) |
| Ultrabot (Template/Example bot)	| Mythrandir		| 21-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/ultrabot) |
| Zonerbot				| Cpt.Guano!		| 11-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/zonerbot) |


## Non-public bots
The following bots are considered non-public. Anonymous users and public developers won't be able to view these bots and change them. If you are a TW Bot Developer you have access to view and modify these bots.

| Name		| Author		| Date (first version) |  Link to source file(s) |
|-----------|---------------|----------------------|-------------------------|
| [DuelBot](DuelBot)       | 2dragons		| 3-1-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/duelbot/) |
| MatchBot	| Mythrandir/Rob Dougan	| 6-5-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/matchbot/) |
| Mrarrogant	| ?			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/mrarrogant/) |
| Octabot	| ?			| 12-7-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/octabot/) |
| Pubbot	| ?			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/pubbot/) |
| Pubhub	| ?			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/pubhub/) |
| Purepubbot	| Cpt.Guano!/qan	| 5-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/purepubbot/) |
| Tournybot	| Sika	(updated by lnx)| 1-7-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/tournybot) |
| [TWDBot](TWDBot)	| ?			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/twdbot/) |
| TWDOpStats	| Maverick		| 02-3-2007	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/twdopstats/) |
| [TWL](TWL)	| lnx			| 27-2-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/twl/) |


## Deleted bots
These bots have been deleted from the repository. We list them here in case a bot needs to be restored.

| Name					| Author		| Date (first version) | Link to source file(s) |
|-----------------------|---------------|----------------------|------------------------|
| [Accessbot](Accessbot)		| ?			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/accessbot@2172) |
| Ballbot				| Nockm			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/ballbot@2386) |
| Estatsbot				| Jacen Solo/Ikrit	| 30-12-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/estatsbot@2417) |
| F1bot					| ?			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/f1bot@2386) |
| Guanobot				| qan			| 9-8-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/guano@2189) |
| Locaterbot				| 2dragons		| 7-8-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/locaterbot@2450) |
| Multibot/ballspec                 	| dugwyler          	| ?         	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/ballspec@2093) |
| Multibot/poker			| D1st0rt		| 16-5-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/poker@2100) |
| Multibot/raceelim			| ?			| ?		| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/raceelim@1576) |
| Multibot/tfrace			| Jacen Solo/Ikrit	| 4-7-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/multibot/tfrace) |
| Powerballbot				| ?			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/powerballbot@2217) |
| Relaybot				| ?			| ?		| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/relaybot@1566) |
| Roboreplacement			| Jacen Solo/Ikrit	| 25-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/roboreplacement@2489) |
| SBBot					| Jacen Solo/Ikrit	| 20-3-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/sbbot@2489) |
| Statsbot				| 2dragons		| 3-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/statsbot@2489) |
| Strikeballbot				| ?			| 11-1-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/strikeballbot@2386) |
| TWBot					| ?			| 18-3-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/twbot@2489) |
| ZHBot					| Jacen Solo/Ikrit	| 24-4-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/zhbot@2489) |
| Pubarrogant				| Jacen Solo/Ikrit	| 20-4-2006	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/pubarrogant@2323) |
| TWDTBot				| ?			| 8-5-2005	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/twdtbot/) |
| TWLBot				| FoN			| 31-5-2004	| [files](https://gitlab.com/trenchwars/twcore/-/tree/master/src/twcore/bots/twlbot@2386) |
